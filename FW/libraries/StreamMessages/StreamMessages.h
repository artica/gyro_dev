/*

* StreamCommandsCRC_h - A Wiring/Arduino library to send/receive commands using
* a generic stream. The commands follow a specific ASCII protocol using CRC and
* the data is validated and retransmitted is necessary.
* Commands use this format:
*
* | Starting Symbol | Data length |    Data    |    CRC   |
* |     1 byte      |   2 bytes   |  x bytes   | 2 bytes  |
*
* The data is transmitetd in ACSII so it cam be human-readable if necessary
* 
* Copyright (C) 2015 Tarquinio Mota
* 
*
* Version 20151015
* 
* This library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this library.  If not, see <http://www.gnu.org/licenses/>.

*/



#ifndef StreamMessages_h
#define StreamMessages_h

#if defined(WIRING) && WIRING >= 100
  #include <Wiring.h>
#elif defined(ARDUINO) && ARDUINO >= 100
  #include <Arduino.h>
#else
  #include <WProgram.h>
#endif
#include <string.h>


// Uncomment this line to enable debugging data on the messages
// #define STREAM_MESSAGES_DEBUG

// Symbol used to represent the beginning of a message
#define MESSAGE_INITIAL_SYMBOL '&'

// Size of the buffer to cache data from the ethernet chip
#define STREAM_BUFFER_SIZE 64

// Different message types. This is always the first byte of the message (after the initial symbol)
#define MESSAGE_TYPE_ACK 'K'
#define MESSAGE_TYPE_CRC_ERROR 'E'

// Uncomment the next line to run the library in debug mode (verbose messages)
//#define SERIALCOMMAND_DEBUG



class StreamMessages 
{
public:
    // Constructor for the class
    // "stream" is the stream we want to use to send/receive the commands
    // "max_message_length" is the maximim lenght to use for the messages 
    StreamMessages( Stream* stream, uint16_t max_message_length);


    // Checks the stream for incoming data. If a full message is received, 
    // the function defined in addMessageReceivedCallback() is called
    void checkStream();


    // Change the source stream to a different stream
    // This is necessary to support the way the Arduino EthernetServer works
    // (It keeps creating new client objects every time there is data available)
    void changeStream(Stream* stream);


    // Adds the callback function to be called when a full command is received
    void addMessageReceivedCallback(void(*function)(char* message, uint16_t length));


    // Send a message through the stream and retransmits it if the message ACK is not received 
    void sendMessage(char* message);

    // Configure the settings for the message retransmission criteria
    // "retries" is the number of times it will try to re-send the message
    // "retry_delay" is the time between each message retry
    // "blocking" is a boolean value indicating if the whole process of sending a message is blocking 
    //            if blocking is active, the processor will lot do anything else untill the ACK message
    //            is received or the maximum number of retries if reached
    void configureRetransmissionSettings(uint8_t retries, uint16_t retry_delay, uint8_t blocking);

    // Returns a pointer to a buffer with the last received message
    char* getLastMessage();

    //Clears the input stream buffer.
    void clearBuffer();


private:

    // Reads the next char from the stream buffer 
    // (the data is read in chunks from the stream to the stream buffer for efficiency)
    char read();

    Stream* _stream;

    enum StreamState {READING_INITIAL_SYMBOL, READING_MESSAGE_TYPE, READING_DATA_LENGTH, READING_DATA, READING_CRC};


    StreamState _state;

    // Pointer to the callback function for a received message
    void (*_callbackMessageReceived)(char* message, uint16_t length);


    // Buffer to store the incoming messages
    char* _message_buffer;

    // Buffer to store the incoming messages
    char _stream_buffer[STREAM_BUFFER_SIZE];
    uint8_t _stream_buffer_pos;
    uint8_t _stream_buffer_size;


    uint8_t _message_type;

    // Maximum lenght for the received messages
    uint16_t _max_message_length;

    // length of the last message received
    uint16_t _current_message_length;

    // CRC value received in the message
    uint16_t _target_crc;

    // Aux variables to calculate the CRC
    uint16_t _sum_1 = 0;
    uint16_t _sum_2 = 0;

    // CRC value calculated locally
    uint16_t _calculated_crc;

    // Current position to write on the message buffer
    uint16_t _current_buffer_position;

    // Return a decimal value from an hexadecimal value
    // returns -1 is the char is not an hexadecimal value
    int8_t getDecimalValue(char c);

    // Sends a hex encoded 16 bit value through the stream
    void sendStreamHexValue(uint16_t value);

};

#endif
