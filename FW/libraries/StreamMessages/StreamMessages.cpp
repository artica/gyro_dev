/*

* StreamCommandsCRC - A Wiring/Arduino library to send/receive commands using
* a generic stream. The commands follow a specific ASCII protocol using CRC and
* the data is validated and retransmitted is necessary.
* Commands use this format:
*
* | Starting Symbol |  Message Type | Data length |    Data    |    CRC   |
* |     1 byte      |     1 byte    |   2 bytes   |  x bytes   | 2 bytes  |
*
* The data is transmitetd in ACSII so it cam be human-readable if necessary
* 
* Copyright (C) 2015 Tarquinio Mota
* 
*
* Version 20151015
* 
* This library is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this library.  If not, see <http://www.gnu.org/licenses/>.

*/


#include "StreamMessages.h"

// Constructor for the class
// "stream" if the stream we want to use to send/receive the commands
// "max_message_length" is the maximim lenght to use for the messages
StreamMessages::StreamMessages( Stream* stream, uint16_t max_message_length )
{
    _stream = stream;

    _max_message_length = max_message_length;

    // Allocate space for the maximum message size +1 terminator byte
    _message_buffer = (char*)malloc(sizeof(char)*max_message_length+1);

    // We start by waiting for the initial symbol
    _state = READING_INITIAL_SYMBOL;

    _stream_buffer_size = 0;
    _stream_buffer_pos = 0;
}


// Change the source stream to a different stream
// This is necessary to support the way the Arduino EthernetServer works
// (It keeps creating new client objects every time there is data available)
void StreamMessages::changeStream(Stream* stream)
{
    _stream = stream;    
}


// Checks the stream for incoming data. If a full message is received, 
// the function defined in addMessageReceivedCallback() is called
void StreamMessages::checkStream()
{
    // Don't check the stream if it's not initialized
    if (_stream == NULL) return;

    // Keep processing the incoming data
    while(true)
    {
        // Try to read a byte from the stream buffer
        char chr = read();

        //if (!chr) splf("THE END IS COMING!");

        // If nothing came out, we're done here!
        if (!chr) return;

        // No matter in what state we are, if we receive the message start symbol we expect to
        // receive the data length immediatlly after
        if ( chr == MESSAGE_INITIAL_SYMBOL)
        {
            _state = READING_MESSAGE_TYPE;
        }

        // If the message in NOT the initial symbol, we need to to different things
        // depending on which point of the message we are 
        else
        {
            // Now we need to do different things depending on the current state
            switch (_state)
            {
                // If we are reading data, copy all the chars to the data buffer (unless it's a special symbol)
                case READING_DATA:
                {
                    #if defined(STREAM_MESSAGES_DEBUG)
                    Serial.println(F("State:READING_DATA"));
                    #endif
                    // Copy the next char to the buffer
                    _message_buffer[_current_buffer_position++] = chr;

                    // Update the crc variables with the new char
                    _sum_1 = (_sum_1 + (uint16_t)chr) % 255;
                    _sum_2 = (_sum_2 + _sum_1) % 255;

                    // Did we reach the end of the message?
                    if (_current_buffer_position >= _current_message_length)
                    {
                        // Add the null terminator do the end of the string
                        _message_buffer[_current_buffer_position++] = 0;

                        // Create the final CRC value (that should match with the received value);
                        _calculated_crc  = (_sum_2 << 8) | _sum_1;

                        // Advance to the next state (Reading the CRC)
                        _state = READING_CRC;
                        _current_buffer_position = 0;

                        #if defined(STREAM_MESSAGES_DEBUG)
                        Serial.print(F("message:")); Serial.println(_message_buffer);
                        Serial.print(F("_calculated_crc:")); Serial.println(_calculated_crc, HEX);
                        #endif
                    }
                }
                break;

                // If we are reafing the CRC, parse the data into a 16-bit value
                case READING_CRC:
                {
                    #if defined(STREAM_MESSAGES_DEBUG)
                    Serial.println(F("State:READING_CRC"));
                    #endif
                    int8_t aux = getDecimalValue(chr);

                    #if defined(STREAM_MESSAGES_DEBUG)
                    Serial.print(F("crc value:")); Serial.println(aux, HEX);
                    #endif
                    _target_crc = _target_crc << 4 | aux;

                    _current_buffer_position++;
                    // Did we read the right amount of chars for the CRC?
                    if (_current_buffer_position >= 4)
                    {

                        if (_calculated_crc == _target_crc)
                        {                            
                            _stream->print(MESSAGE_INITIAL_SYMBOL);
                            _stream->print(MESSAGE_TYPE_ACK, HEX);
                            sendStreamHexValue( _calculated_crc );

                            (*_callbackMessageReceived)(_message_buffer, _current_message_length);
                        }
                        else
                        {
                            _stream->print(MESSAGE_INITIAL_SYMBOL);
                            _stream->print(MESSAGE_TYPE_CRC_ERROR);
                        }

                        _state = READING_INITIAL_SYMBOL;
                    }
                }
                break;

                // If we are reading the message length (hex encoded value), we need to read 4 chars (16 bit value)
                case READING_DATA_LENGTH:
                {
                    #if defined(STREAM_MESSAGES_DEBUG)
                    Serial.println(F("State:READING_DATA_LENGTH"));
                    #endif
                    int8_t aux = getDecimalValue(chr);

                    //Serial.print(F("len value:")); Serial.println(aux);

                    _current_message_length = _current_message_length << 4 | aux;

                    _current_buffer_position++;
                    // Did we read the right amount of chars for the message size?
                    if (_current_buffer_position >= 4)
                    {
                        #if defined(STREAM_MESSAGES_DEBUG)
                        Serial.print(F("length:")); Serial.println(_current_message_length);
                        #endif
                        _state = READING_DATA;
                        _current_buffer_position = 0;
                        _target_crc;
                        _calculated_crc = 0xFFFF;

                        _sum_1 = 0;
                        _sum_2 = 0;
                    }
                }
                break;

                // The message type is a single HEX character indicating the message data type
                case READING_MESSAGE_TYPE:
                {
                    #if defined(STREAM_MESSAGES_DEBUG)
                    Serial.println(F("State:READING_MESSAGE_TYPE"));
                    #endif
                    _message_type = getDecimalValue(chr);
                    #if defined(STREAM_MESSAGES_DEBUG)
                    Serial.print(F("_message_type:")); Serial.println(_message_type);
                    #endif

                    _state = READING_DATA_LENGTH;
                    _current_buffer_position = 0;
                    _current_message_length = 0;
                }
                break;
            }
        }
    }
}


// Sends a hex encoded 16 bit value through the stream
void StreamMessages::sendStreamHexValue(uint16_t value)
{
    if (value < 0x1000) _stream->println('0');
    if (value < 0x100) _stream->println('0');
    if (value < 0x10) _stream->println('0');
    _stream->println(value, HEX);
}


// Adds the callback function to be called when a full command is received
int8_t StreamMessages::getDecimalValue(char c)
{
    if (c>='0' && c<='9') return c-'0';
    if (c>='A' && c<='Z') return c-'A'+10;
    if (c>='a' && c<='z') return c-'a'+10;
    return -1;
}


// Adds the callback function to be called when a full command is received
void StreamMessages::addMessageReceivedCallback(void(*function)(char* message, uint16_t length))
{
    _callbackMessageReceived = function;
}


// Returns a pointer to a buffer with the last received message
char* StreamMessages::StreamMessages::getLastMessage()
{
    return _message_buffer;
}



// Send a message through the stream and retransmits it if the message ACK is not received 
void StreamMessages::sendMessage(char* message)
{
    
}


// Reads the next char from the stream buffer 
// (the data is read in chunks from the stream to the stream buffer for efficiency)
char StreamMessages::read()
{
    // Do we have data in the buffer?
    if (_stream_buffer_pos < _stream_buffer_size)
    {
        // We do, so we just return the next char in the buffer
        return _stream_buffer[_stream_buffer_pos++];
    }

    // Seems the buffer is empty... Is there any data to read on the stream?
    size_t data_size = _stream->available();

    #if defined(STREAM_MESSAGES_DEBUG)
    Serial.print(F("_stream->available() = ")); Serial.println(_stream->available());
    #endif
    if (data_size)
    {
        // Read bytes from the existing data up to the buffer size
        if (data_size > STREAM_BUFFER_SIZE) data_size = STREAM_BUFFER_SIZE;

        size_t data_read = _stream->readBytes((uint8_t*)_stream_buffer, data_size);

        // Set the pointers so we know where to read from next time we get to this method
        _stream_buffer_size = data_read;
        _stream_buffer_pos = 1;
        // Finally, return the first position of the buffer        
        return _stream_buffer[0];
    }

    // If we got here, there is no data to read... :(
    //splf("No data!"); //(delay(20));
    return 0;
}


// Configure the settings for the message retransmission criteria
// "retries" is the number of times it will try to re-send the message
// "retry_delay" is the time between each message retry
// "blocking" is a boolean value indicating if the whole process of sending a message is blocking 
//            if blocking is active, the processor will lot do anything else untill the ACK message
//            is received or the maximum number of retries if reached
void configureRetransmissionSettings(uint8_t retries, uint16_t retry_delay, uint8_t blocking)
{
    // @todo
}


//Clears the input stream buffer.
void StreamMessages::clearBuffer()
{
    //@todo
}


