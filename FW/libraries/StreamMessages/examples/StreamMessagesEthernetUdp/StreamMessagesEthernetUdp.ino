// Demo for the StreamMessages library using an Ethernet connection

#include <StreamMessages.h>

#include <SPI.h>
#include <Ethernet2.h>
#include <EthernetUdp2.h>

/*
	@todo ADAPT THIS TO THE UDP LIBRARY
	@todo ADAPT THIS TO THE UDP LIBRARY
	@todo ADAPT THIS TO THE UDP LIBRARY
	@todo ADAPT THIS TO THE UDP LIBRARY
	@todo ADAPT THIS TO THE UDP LIBRARY

	While this library works fine using the normal Ethernet and Ethernet2 libraries
	it can be much more efficient by doing a simple modification to the Arduino classes:
	This makes the stream able to read data in blocks instead on 1 byte each time
	
	in file EthernetClient.h add:

	size_t readBytes( char *buffer, size_t size);


	in file EthernetClient.cpp add:

	size_t EthernetClient::readBytes(char *buf, size_t size) 
	{
	  return recv(_sock, (uint8_t *)buf, size);
	}

	in file Stream.h add "virtual" before the same methods:

	size_t readBytes( char *buffer, ........
	size_t readBytes( uint8_t *buffer, .....

	become:

	virtual size_t readBytes( char *buffer, ........
	virtual size_t readBytes( uint8_t *buffer, .....

	There are several Stream.h files, depending on the arduino type in use (avr, sam, samd)
	Make sure you use the one mathing your arduino type (or just change them all :)

*/

// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network.
// gateway and subnet are optional:
byte mac[] = {  0xDE, 0xAD, 0xBE, 0x12, 0x34, 0x56 };
IPAddress ip(192, 168, 5, 77);
IPAddress gateway(192,168,1, 1);
IPAddress subnet(255, 255, 255, 0);


// An EthernetUDP instance to let us send and receive packets over UDP
EthernetUDP Udp;


// Create a Stream Parser without a stream for now...
// Every time an EthernetClient is created, it will be passed to the parser as 
// the stream to use. This is a way to get around the Ethernet library limitations.
StreamMessages stream_parser( NULL, 2048 );


// Callback for when a correct message is received (including CRC validation)
void messageReceived(char* message, uint16_t length)
{
    Serial.print(F("Message:")); Serial.println(message);
}


void setup()
{
    // Initialize the serial port normally
    Serial.begin(115200);

	pinMode(13, OUTPUT);

	// Adds the callback function to be called when a correct message is received
	stream_parser.addMessageReceivedCallback(messageReceived);

	// initialize the ethernet device
	Ethernet.begin(mac, ip, gateway, gateway, subnet);

	// Initialize the Udp server and pass it's data stream to the stream parser
    Udp.begin(8888);
    stream_parser.changeStream(&Udp);

	Serial.print("UDP Server address:");
	Serial.println(Ethernet.localIP());

    Serial.println(F("Setup complete!"));
}


void loop() 
{
	// Check if there is any UDP message
    int packetSize = Udp.parsePacket();

    // Is there any data to read?
    if (packetSize) 
    {
    	digitalWrite(13, HIGH);
        stream_parser.checkStream();        
	    digitalWrite(13, LOW);
    }    

}