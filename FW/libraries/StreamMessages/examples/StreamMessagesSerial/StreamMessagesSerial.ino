// Demo Code for SerialCommand Library
// Steven Cogswell
// May 2011

#include <StreamMessages.h>

// Constructor for the class
// "stream" is the stream we want to use to send/receive the commands
// "max_message_length" is the maximim lenght to use for the messages 
// StreamMessages( Stream* stream, uint16_t max_message_length);
StreamMessages stream_parser(&Serial, 64);


// Callback for when a correct message is received (including CRC validation)
void messageReceived(char* message, uint16_t length)
{
    Serial.print(F("Message:")); Serial.println(message);
}


void setup()
{
    // Initialize the serial port normally
    Serial.begin(115200);

    // Adds the callback function to be called when a correct message is received
    stream_parser.addMessageReceivedCallback(messageReceived);

    Serial.println(F("Setup complete!"));
}


void loop() 
{
    // This method needs to be called often in the loop
    stream_parser.checkStream();
}

