#ifndef _MY_GENERIC_UTILS_H
#define _MY_GENERIC_UTILS_H

#include <stdint.h>

#include "Arduino.h"
//#include "DebugConfig.h"

#define SERIAL_PORT Serial

// Macros to shorten the Serial print calls
#define sp(text) SERIAL_PORT.print(text)
#define spl(text) SERIAL_PORT.println(text)
#define spf(text) SERIAL_PORT.print(F(text))
#define splf(text) SERIAL_PORT.println(F(text))

#define spx(text) SERIAL_PORT.print(text, HEX)
#define splx(text) SERIAL_PORT.println(text, HEX)
#define spb(text) SERIAL_PORT.print(text, BIN)
#define splb(text) SERIAL_PORT.println(text, BIN)
#define spd(text) SERIAL_PORT.print(text, DEC)
#define spld(text) SERIAL_PORT.println(text, DEC)
#define spvar(var) { SERIAL_PORT.print(F(#var"=")); SERIAL_PORT.print(var); SERIAL_PORT.print(F("  ")); }
#define splvar(var) { SERIAL_PORT.print(F(#var"=")); SERIAL_PORT.println(var); }
#define splvarb(var) { SERIAL_PORT.print(F(#var"=")); SERIAL_PORT.println(var, BIN); }
#define splvarx(var) { SERIAL_PORT.print(F(#var"=")); SERIAL_PORT.println(var, HEX); }


// Macros used to simplify the declaration of variables and prints
// Use: P(my_var) = "Hello";
#define P(name)  static const char name[] PROGMEM


// Convert a hexadecimal character to a numerical value
uint8_t hex2byte(char hex);


// ---------------------------------------------------------------
// Functions to red/write different data types from/to byte arrays
// ---------------------------------------------------------------

// Extract a 16 bit signed int from a specific position in a byte array
int16_t getInt16FromArray(uint8_t* buffer, uint16_t pos);

// Extract a 32 bit unsigned int from a specific position in a byte array
uint32_t getUInt32FromArray(uint8_t* buffer, uint16_t pos);

// Extract a 32 bit signed int from a specific position in a byte array
int32_t getInt32FromArray(uint8_t* buffer, uint16_t pos);



// Writes a 32 bit signed int to a byte buffer in a specific position
void putInt32InArray(int32_t value, uint8_t* buffer, uint16_t pos);



// ----------------------------------------------------------------------------
// Functions to read/write different data types from/to HEX encoded char arrays
// ----------------------------------------------------------------------------

// Parse 2 chars of an array as a 8 bit signed hexadecimal number
int8_t parseInt8FromArray(char* buffer, uint16_t pos);

// Parse 2 chars of an array as a 8 bit unsigned hexadecimal number
uint8_t parseUInt8FromArray(char* buffer, uint16_t pos);

// Parse 4 chars of an array as a 16 bit signed hexadecimal number
int16_t parseInt16FromArray(char* buffer, uint16_t pos);

// Parse 4 chars of an array as a 16 bit unsigned hexadecimal number
uint16_t parseUInt16FromArray(char* buffer, uint16_t pos);

// Parse 8 chars of an array as a 32 bit signed hexadecimal number
int32_t parseInt32FromArray(char* buffer, uint16_t pos);

// Parse 8 chars of an array as a 32 bit unsigned hexadecimal number
uint32_t parseUInt32FromArray(char* buffer, uint16_t pos);



// Prints as array of bytes as numerical values
void printArray(byte* array, byte length);



// ----------------------------------------------------------------------------
//                        Time measure functions
// ----------------------------------------------------------------------------

// Start a timer to measure the time (in microseconds);
void startTimer();
// Print how much time has passed since startTimer() was called
void checkTimer();


// Set the loop to a specific framerate
// This works by calculating the time each loop took to execute, and then 
// performing a delay of the remaining necessary time for the desired framerate
// Set to 0 to disable
void setFramerate(uint16_t rate);

// This needs to be called in the end of every loop to adjust the framerate
void adjustFrameRate();


// ----------------------------------------------------------------------------
//                           RAM debug functions
// ----------------------------------------------------------------------------

// Get the current free ram value
int getFreeRam();

// Prints the current free ram to the Serial port
void printFreeRam();


#endif
