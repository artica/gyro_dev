#include "Utils.h"


// Convert a hexadecimal character to a numerical value
uint8_t hex2byte(char hex)
{
	if (hex>='0' && hex<='9') return hex-'0'; 
	if (hex>='A' && hex<='F') return hex-'A'+10; 
	if (hex>='a' && hex<='f') return hex-'a'+10;
	return 0;
}


// ---------------------------------------------------------------
// Functions to red/write different data types from/to byte arrays
// ---------------------------------------------------------------

// Extract a 16 bit signed int from a specific position in a byte array
int16_t getInt16FromArray(uint8_t* buffer, uint16_t pos)
{
	return (int16_t)buffer[pos]<<8 | (int16_t)buffer[pos+1];
}


// Extract a 32 bit unsigned int from a specific position in a byte array
uint32_t getUInt32FromArray(uint8_t* buffer, uint16_t pos)
{
	unsigned long value =	(uint32_t)buffer[pos]<<24 | 
							(uint32_t)buffer[pos+1]<<16 | 
							(uint32_t)buffer[pos+2]<<8 | 
							(uint32_t)buffer[pos+3];
    return value;
}


// Extract a 32 bit signed int from a specific position in a byte array
int32_t getInt32FromArray(uint8_t* buffer, uint16_t pos)
{
	return 	(uint32_t)buffer[pos] << 24 | 
			(uint32_t)buffer[pos+1] << 16 | 
			(uint32_t)buffer[pos+2] << 8 | 
			(uint32_t)buffer[pos+3];
}


// Writes a 32 bit signed int to a byte buffer in a specific position
void putInt32InArray(int32_t value, uint8_t* buffer, uint16_t pos)
{
	buffer[pos] = value >> 24;
	buffer[pos+1] = value >> 16;
	buffer[pos+2] = value >> 8;
	buffer[pos+3] = value;
}



// ----------------------------------------------------------------------------
// Functions to read/write different data types from/to HEX encoded char arrays
// ----------------------------------------------------------------------------

// Parse 2 chars of an array as a 8 bit signed hexadecimal number
int8_t parseInt8FromArray(char* buffer, uint16_t pos)
{
	return hex2byte(buffer[pos])<<4 | hex2byte(buffer[pos+1]);
}


// Parse 2 chars of an array as a 8 bit unsigned hexadecimal number
uint8_t parseUInt8FromArray(char* buffer, uint16_t pos)
{
	return (uint8_t)hex2byte(buffer[pos])<<4 | (uint8_t)hex2byte(buffer[pos+1]);
}

// Parse 4 chars of an array as a 8 bit signed hexadecimal number
int16_t parseInt16FromArray(char* buffer, uint16_t pos)
{
	return 	(int16_t)parseUInt8FromArray( buffer, pos) << 8 |
			(int16_t)parseUInt8FromArray( buffer, pos+2) ;
}


// Parse 4 chars of an array as a 16 bit unsigned hexadecimal number
uint16_t parseUInt16FromArray(char* buffer, uint16_t pos)
{
	return 	(uint16_t)parseUInt8FromArray( buffer, pos) << 8 | 
			(uint16_t)parseUInt8FromArray( buffer, pos+2);
}


// Parse 8 chars of an array as a 32 bit unsigned hexadecimal number
int32_t parseInt32FromArray(char* buffer, uint16_t pos)
{
	return 	(int32_t)parseUInt8FromArray( buffer, pos) << 24 |
			(int32_t)parseUInt8FromArray( buffer, pos+2) << 16 | 
			(int32_t)parseUInt8FromArray( buffer, pos+4) << 8 | 
			(int32_t)parseUInt8FromArray( buffer, pos+6);
}


// Parse 8 chars of an array as a 32 bit unsigned hexadecimal number
uint32_t parseUInt32FromArray(char* buffer, uint16_t pos)
{
	return 	(uint32_t)parseUInt8FromArray( buffer, pos) << 24 |
			(uint32_t)parseUInt8FromArray( buffer, pos+2) << 16 | 
			(uint32_t)parseUInt8FromArray( buffer, pos+4) << 8 | 
			(uint32_t)parseUInt8FromArray( buffer, pos+6);
}



// Prints an array of byte values
void printArray(byte* array, byte length)
{
	spf("array:");
	for (byte i=0; i<length; i++)
	{
		sp(array[i]); sp(' ');
	}
	spl();
}





// ----------------------------------------------------------------------------
//                        Time measure debug functions
// ----------------------------------------------------------------------------


// Global variable necessary for the debug timer
uint32_t _my_debug_timer;

// Start a timer to measure the time (in microseconds);
void startTimer()
{
	_my_debug_timer = micros();
}


// Print how much time has passed since startTimer() was called
void checkTimer()
{
	uint32_t end_time = micros();
	uint32_t elapsed = end_time - _my_debug_timer;

	spf("Timer(us):"); sp(elapsed); spf(" (ms):"); sp(elapsed/1000); spf(" (s):"); spl(elapsed/1000000);
}


// Global variables used for the framerate functions
float _frame_time = 0;
unsigned long _frame_start_time = 0;

// Set the loop to a specific framerate
// This works by calculating the time each loop took to execute, and then 
// performing a delay of the remaining necessary time for the desired framerate
// Set to 0 to disable
void setFramerate(uint16_t rate)
{
    if (rate==0) _frame_time = 0;
    else _frame_time = 1000/rate;
}


// This needs to be called in the end of every loop to adjust the framerate
void adjustFrameRate()
{
    // If the delay between frames is 0, no point in checking the time...
    if (_frame_time==0) return;
    // Keep waiting untill we we reach the desired time for each frame
    while (millis()-_frame_start_time < _frame_time);
    _frame_start_time = millis();
}


// ----------------------------------------------------------------------------
//                           RAM debug functions
// ----------------------------------------------------------------------------


// Get the current free ram value
int getFreeRam()
{
	// Get the memory position for the beginning of the heap
	extern int __heap_start, *__brkval;

	// Declare a variable (it will be on the top of the stack)
	int v; 

	// Now we return the difference between the memory position of the variable we just 
	// declared and the memory position for the start of the heap
	// The space in the middle should be the free RAM
	return (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval); 
}

// Prints the current free ram
void printFreeRam()
{
	Serial.print(F("Free RAM:"));
	Serial.println(getFreeRam());
}

