/*
 Artica CC - http://artica.cc

 Gyro Bumper V0.1 - Serras and Tarquinio

 This Software is under The MIT License (MIT)

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

// Update States for "later" reply (to do not block the Wire Interrupt driven Comm)
#define _DEBUG_BUMPER_ false

#define CMD_DATA	1
#define SHORT_DATA 	2
#define LONG_DATA 	4
#define FLOAT_DATA 	4

#define _LEFT 0
#define _CENTER 1
#define _RIGHT 2

// ------------------------------------------------------------------------------
// PARSER: Received an I2C "wire" message from the master - CRC already tested - MUST BE FAST!
// ------------------------------------------------------------------------------
void CommParseCommand(unsigned short howMany) {

	readyFlagToReply = false;

	// REPEAT THE ORDER ON THE SEND BUFFER
	OrderRequestBumper = (enum _ReceivedOrderBumper) receivedBuffer[0];

	// Repeat command to SendBuffer
	sendBuffer[0] = OrderRequestBumper;

	if (_DEBUG_BUMPER_) {
		Serial.print("CMD:");
		Serial.println((unsigned char) OrderRequestBumper);
	}

	switch (OrderRequestBumper) {

	case RX_NONE:
		CommReplyLaterBumper = NO_RX;
		break;

	case CFG:
		CommReplyLaterBumper = REPLY_CFG;
		break;

	case GLRED:
		CommReplyLaterBumper = REPLY_GLRED;
		break;

	case GLGREEN:
		CommReplyLaterBumper = REPLY_GLGREEN;
		break;

	case GLBLUE:
		CommReplyLaterBumper = REPLY_GLBLUE;
		break;

	case GLAMBIENT:
		CommReplyLaterBumper = REPLY_GLAMBIENT;
		break;

	case GLPROXIMITY:
		CommReplyLaterBumper = REPLY_GLPROXIMITY;
		break;

	case GLGESTURE:
		CommReplyLaterBumper = REPLY_GLGESTURE;
		break;

	case GLLINE:
		CommReplyLaterBumper = REPLY_GLLINE;
		break;

	case GRRED:
		CommReplyLaterBumper = REPLY_GRRED;
		break;

	case GRGREEN:
		CommReplyLaterBumper = REPLY_GRGREEN;
		break;

	case GRBLUE:
		CommReplyLaterBumper = REPLY_GRBLUE;
		break;

	case GRAMBIENT:
		CommReplyLaterBumper = REPLY_GRAMBIENT;
		break;

	case GRPROXIMITY:
		CommReplyLaterBumper = REPLY_GRPROXIMITY;
		break;

	case GRGESTURE:
		CommReplyLaterBumper = REPLY_GRGESTURE;
		break;

	case GRLINE:
		CommReplyLaterBumper = REPLY_GRLINE;
		break;

	case GCRED:
		CommReplyLaterBumper = REPLY_GCRED;
		break;

	case GCGREEN:
		CommReplyLaterBumper = REPLY_GCGREEN;
		break;

	case GCBLUE:
		CommReplyLaterBumper = REPLY_GCBLUE;
		break;

	case GCAMBIENT:
		CommReplyLaterBumper = REPLY_GCAMBIENT;
		break;

	case GCPROXIMITY:
		CommReplyLaterBumper = REPLY_GCPROXIMITY;
		break;

	case GCGESTURE:
		CommReplyLaterBumper = REPLY_GCGESTURE;
		break;

	case SRGB:
		CommReplyLaterBumper = REPLY_SRGB;
		break;

		// the shit just hit the fan
	default:
		Serial.println("Camandro!");
		break;

	}

}

