/*
 Artica CC - http://artica.cc

 Gyro Bumper V0.1 - Serras and Tarquinio

 This Software is under The MIT License (MIT)

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

void InitializeSensors(void) {
bool somethingWrong = false;

	//Serial.println("a");

	TurnOffSensors();

	//Serial.println("b");

	for (int count = 0; count < 3; count++) {

		delay(10);

		somethingWrong = false;

		if (config[count] != SENSOR_OFF) {//continue;

		// Initialize APDS-9960 (configure I2C and initial values)
		if (apds[count].init(pins[count * 2], pins[count * 2 + 1])) {

			delay(10);

			Serial.print("cnt:"); Serial.println(count);

			// CONFIG
			switch (config[count])
			{
			case SENSOR_PROXIMITY:

				// Start running the APDS-9960 light sensor (no interrupts)
				if (apds[count].enableLightSensor(false)) {

					Serial.print(words[count]);
					Serial.println("Light sensor is now running");
				}
				else
				{
					Serial.print("Something went wrong during");
					Serial.print(words[count]);
					Serial.println(" enable light sensor!");
					somethingWrong = true;

				}

				// Adjust the Proximity sensor gain
				if (!apds[count].setProximityGain(PGAIN_8X)) {
					Serial.println("Something went wrong trying to set PGAIN");
					somethingWrong = true;
				}

				// Start running the APDS-9960 proximity sensor engine
				if (!apds[count].enableProximitySensor(false)) {
					Serial.print("Failed Proximity");
					somethingWrong = true;
				}


				break;

			case SENSOR_GESTURE:
				Serial.print("GESTURE:");Serial.println(words[count]);
				// Start running the APDS-9960 gesture sensor engine
				if ( !apds[count].enableGestureSensor(false) ) {
					Serial.println(F("Gesture failed"));
					somethingWrong = true;
				}

				break;

			case SENSOR_RGB:
				Serial.print("RGB:");Serial.println(words[count]);
				// Start running the APDS-9960 light sensor (no interrupts)
				if (apds[count].enableLightSensor(false)) {

					Serial.print(words[count]);
					Serial.println("Light sensor is now running");

				}
				else
				{
					Serial.print("Something went wrong during ");
					Serial.print(words[count]);
					Serial.println(" enable light sensor!");
					somethingWrong = true;

				}
				break;

			default:
				somethingWrong = true;
				break;
			}


		} else {
			Serial.print("Something went wrong during ");
			Serial.print(words[count]);
			Serial.println(" APDS-9960 init!");



			somethingWrong = true;
		}
		}

		// If something went wrong turn off the sensor
		if (somethingWrong) config[count] = SENSOR_OFF;



	}


}

void updateData(void)
{

	for (int count = 0; count < 3; count++) {

		if (config[count] != SENSOR_OFF) {
			UpdateComm();
			apds[count].readAmbientLight(ambientLight[count]);
			UpdateComm();
			apds[count].readRedLight(redLight[count]);
			UpdateComm();
			apds[count].readGreenLight(greenLight[count]);
			UpdateComm();
			apds[count].readBlueLight(blueLight[count]);
			UpdateComm();

			readProximity(count);
			UpdateComm();
			readGesture(count);
			UpdateComm();


//						  Serial.print(words[count]);
//						  Serial.print("\tR:");
//						  Serial.print(redLight[count]);
//						  Serial.print("\tG:");
//						  Serial.print(greenLight[count]);
//						  Serial.print("\tB:");
//						  Serial.print(blueLight[count]);
//						  Serial.print("\tA:");
//						  Serial.print(ambientLight[count]);
//						  Serial.print("\tP:");
//						  Serial.print(proximity[count]);
//						  Serial.println("\t");
		}

	}
}

void readProximity(int counter) {
	if (config[counter] == SENSOR_PROXIMITY)
	{
		apds[counter].readProximity(proximity[counter]);
	}
	else
	{
		proximity[counter] = 0;
	}
}

void readGesture(int counter) {

	if (config[counter] == SENSOR_GESTURE)
	{
		// Read Gesture
		if ( apds[counter].isGestureAvailable() ) {
		  gesture[counter] = apds[counter].readGesture();
		}

	} else
	{
		gesture[counter] = 0;
	}

}

void TurnOffSensors(void) {
	for (int count = 0; count < 3; count++) {



		//apds[count].disableLightSensor();
		//apds[count].disableProximitySensor();
//
		Serial.println("a1");


		pinMode(pins[count * 2], OUTPUT);
		pinMode(pins[count * 2] + 1, OUTPUT);

		digitalWrite(pins[count * 2], HIGH);
		digitalWrite(pins[count * 2] + 1, HIGH);

		delay (5);

		pinMode(pins[count * 2], INPUT_PULLUP);
		pinMode(pins[count * 2] + 1, INPUT_PULLUP);



		//apds[count].disablePower();

		//apds[count].enablePower();
		//apds[count].disableGestureSensor();
	}
}

unsigned short getRightLine() {
	unsigned short value = 0;

	Serial.println("r");

	pinMode(PinLinePWR, OUTPUT);
	digitalWrite(PinLinePWR, LOW);
	delayMicroseconds(700);
	value = analogRead(PinLineRight);
	digitalWrite(PinLinePWR, HIGH);

	return value;
}

unsigned short getLeftLine() {
	unsigned short value = 0;

	pinMode(PinLinePWR, OUTPUT);
	digitalWrite(PinLinePWR, LOW);
	delayMicroseconds(700);
	value = analogRead(PinLineLeft);
	digitalWrite(PinLinePWR, HIGH);

	return value;
}

