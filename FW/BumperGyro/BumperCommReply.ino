/*
 Artica CC - http://artica.cc

 Gyro Bumper V0.1 - Serras and Tarquinio

 This Software is under The MIT License (MIT)

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

volatile unsigned char buffIndex = 0;

// ------------------------------------------------------------------------------
// Buffer prepare
// ------------------------------------------------------------------------------

void startComm() {
	buffIndex = 1;		// Take Command already filled into account
	sendBuffer[1] = 0;	// DUMMY
}

void addCharData(char reply) {
	sendBuffer[buffIndex] = (char) reply;

	buffIndex = buffIndex + 1;
}

void addShortData(short data) {
	for (unsigned char i = buffIndex; i < buffIndex + 2; i++)
		sendBuffer[i] = 0x00FF & (data >> ((i - buffIndex) * 8));

	buffIndex = buffIndex + 2;
}

void addLongData(long data) {
	for (unsigned char i = buffIndex; i < buffIndex + 4; i++)
		sendBuffer[i] = 0x000000FF & (data >> ((i - buffIndex) * 8));

	buffIndex = buffIndex + 4;
}

void addFloatData(float data) {
	char * FloatData;
	FloatData = (char*) &data;

	for (unsigned char i = buffIndex; i < buffIndex + 4; i++)
		sendBuffer[i] = FloatData[i - buffIndex];

	buffIndex = buffIndex + 4;

}

// ------------------------------------------------------------------------------
// Activate Flag Reply
// ------------------------------------------------------------------------------
// ------------ Prepare the Reply with data --------------
void replyData(unsigned char bufferSize) {
	CommReplyLaterBumper = NO_RX;
	sendLength = bufferSize;					// Buffer Size
	readyFlagToReply = true;
}
// ------------ Prepare the Reply as true --------------
void replyTrue(void) {
	sendBuffer[1] = TRUE;
	sendLength = 1 + 1;				// Buffer Size = 1 (cmd) + 1 (data = TRUE)
	CommReplyLaterBumper = NO_RX;
	readyFlagToReply = true;
}
// ------------ Prepare the Reply as false --------------
void replyFalse(void) {
	sendBuffer[1] = FALSE;
	sendLength = 1 + 1;				// Buffer Size = (cmd) + 1 (data = FALSE)
	CommReplyLaterBumper = NO_RX;
	readyFlagToReply = true;
}

// ------------------------------------------------------------------------------
// Pooling event to REPLY ORDERS updated - can be interrupted
// ------------------------------------------------------------------------------
void UpdateComm() {
	// REPLY TO MASTER (the received "Wire" Orders)
	switch (CommReplyLaterBumper) {

	// Dummy state
	case NO_REPLY:
		break;

	// Config State
	case REPLY_CFG:
		receivedValue = (unsigned short) getShortData(1);

		Serial.println(receivedValue, HEX);

		for (int count = 0; count < 3; count++)
		{
			config[count] = (_BumperConfig) (receivedValue & 0x0F);
			receivedValue = receivedValue >> 4;

			Serial.print(words[count]);
			Serial.print(":");
			Serial.print((unsigned short) config[count], HEX);
			Serial.print(" ");
		}

		Serial.println("");

		InitializeSensors();

		replyTrue();

		break;

	case REPLY_GLRED:
		startComm();
		addShortData(redLight[_LEFT]);
		replyData(CMD_DATA + SHORT_DATA);
		break;

	case REPLY_GLGREEN:
		startComm();
		addShortData(greenLight[_LEFT]);
		replyData(CMD_DATA + SHORT_DATA);
		break;

	case REPLY_GLBLUE:
		startComm();
		addShortData(blueLight[_LEFT]);
		replyData(CMD_DATA + SHORT_DATA);
		break;

	case REPLY_GLAMBIENT:
		startComm();
		addShortData(ambientLight[_LEFT]);
		replyData(CMD_DATA + SHORT_DATA);
		break;

	case REPLY_GLPROXIMITY:
		startComm();
		addShortData(proximity[_LEFT]);
		replyData(CMD_DATA + SHORT_DATA);
		break;

	case REPLY_GLGESTURE:
		startComm();
		addShortData(gesture[_LEFT]);
		replyData(CMD_DATA + SHORT_DATA);
		break;

	case REPLY_GLLINE:
		startComm();
		addShortData(getLeftLine());
		replyData(CMD_DATA + SHORT_DATA);
		break;

	case REPLY_GRRED:
		startComm();
		addShortData(redLight[_RIGHT]);
		replyData(CMD_DATA + SHORT_DATA);
		break;

	case REPLY_GRGREEN:
		startComm();
		addShortData(greenLight[_RIGHT]);
		replyData(CMD_DATA + SHORT_DATA);
		break;

	case REPLY_GRBLUE:
		startComm();
		addShortData(blueLight[_RIGHT]);
		replyData(CMD_DATA + SHORT_DATA);
		break;

	case REPLY_GRAMBIENT:
		startComm();
		addShortData(ambientLight[_RIGHT]);
		replyData(CMD_DATA + SHORT_DATA);
		break;

	case REPLY_GRPROXIMITY:
		startComm();
		addShortData(proximity[_RIGHT]);
		replyData(CMD_DATA + SHORT_DATA);
		break;

	case REPLY_GRGESTURE:
		startComm();
		addShortData(gesture[_RIGHT]);
		replyData(CMD_DATA + SHORT_DATA);
		break;

	case REPLY_GRLINE:
		startComm();
		addShortData(getRightLine());
		replyData(CMD_DATA + SHORT_DATA);
		break;

	case REPLY_GCRED:
		startComm();
		addShortData(redLight[_CENTER]);
		replyData(CMD_DATA + SHORT_DATA);
		break;

	case REPLY_GCGREEN:
		startComm();
		addShortData(greenLight[_CENTER]);
		replyData(CMD_DATA + SHORT_DATA);
		break;

	case REPLY_GCBLUE:
		startComm();
		addShortData(blueLight[_CENTER]);
		replyData(CMD_DATA + SHORT_DATA);
		break;

	case REPLY_GCAMBIENT:
		startComm();
		addShortData(ambientLight[_CENTER]);
		replyData(CMD_DATA + SHORT_DATA);
		break;

	case REPLY_GCPROXIMITY:
		startComm();
		addShortData(proximity[_CENTER]);
		replyData(CMD_DATA + SHORT_DATA);
		break;

	case REPLY_GCGESTURE:
		startComm();
		addShortData(gesture[_CENTER]);
		replyData(CMD_DATA + SHORT_DATA);
		break;

	case REPLY_SRGB:
		analogWrite(PinR, (unsigned char) getCharData(1));
		analogWrite(PinG, (unsigned char) getCharData(2));
		analogWrite(PinB, (unsigned char) getCharData(3));
		replyTrue();
		break;

	// The shit just hit the fan
	default:
		break;
	};
}

// BUFFER AUX FUNCTIONS
char getCharData(unsigned char index) {
	return receivedBuffer[index];
}

short getShortData(unsigned char index) {
	short Data = 0;
	char *Padding = (char*) &Data;

	for (unsigned char i = index; i < index + 2; i++) {
		Padding[i - index] = ((char) receivedBuffer[i]);
	}
	return Data;
}

long getLongData(unsigned char index) {
	long Data = 0;
	char *Padding = (char*) &Data;

	for (unsigned char i = index; i < index + 4; i++) {
		Padding[i - index] = ((char) receivedBuffer[i]);
	}
	return Data;
}

float getFloatData(unsigned char index) {
	float reply = 0;

	char *Padding = (char*) &reply;

	for (unsigned char i = index; i < index + 4; i++) {
		Padding[i - index] = ((char) receivedBuffer[i]);
	}
	return reply;
}

