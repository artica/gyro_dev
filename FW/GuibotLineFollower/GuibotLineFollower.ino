/*
 Artica CC - http://artica.cc

 Gyro Sensors - Serras and Tarquinio

 This App is mainly to serve as a start point - to test the Gyro Functions

 This Software is under The MIT License (MIT)

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

#include <Gyro.h>
#include <Motoruino2.h>
#include <Wire.h>
#include <Metro.h>
#include <SPI.h>
#include <Servo.h>

#define DEBUG_PIN 13

/*********************************************

  DEBOUNCER VARS

********************************************/

int bt1 = 2;         // the number of the input pin
int state1 = HIGH;      // the current state1 of the output pin
int reading1;           // the current reading1 from the input pin
int previous1 = LOW;    // the previous1 reading1 from the input pin
long time1 = 0;         // the last time1 the output pin was toggled
long debounce1 = 200;   // the  time1, increase if the output flickers

/*********************************************

  MOTORS

********************************************/

// Define the rotation (chose between 1 or -1)
#define M1_SIGNAL -1
#define M2_SIGNAL 1


/*********************************************

  GYRO

********************************************/

Gyro gyro;

Metro sensorsCheck_metro = Metro(25);
Metro print_metro = Metro(500);

//Metro RGBled_metro = Metro (50);
//Metro Eyes_metro = Metro (5);

//Metro RED_active_metro = Metro (250);

int debug = 0;

/*********************************************

  SENSORS VARS

********************************************/
unsigned short leftProx, leftRed, leftGreen, leftBlue, leftAmbient, leftLine, leftGesture;
unsigned short rightProx, rightRed, rightGreen, rightBlue, rightAmbient, rightLine, rightGesture;
unsigned short centerProx, centerRed, centerGreen, centerBlue, centerAmbient, centerGesture;

unsigned short value, heading;
unsigned int count = 0;

int current;
int battery;

int leftBumper;
int rightBumper;

int leftDistance;
int rightDistance;
int centerDistance;

int leftAmbientLight;
int rightAmbientLight;
int centerAmbientLight;

int leftLineValue; 
int rightLineValue;

int centerLineValue, centerLineOutput, centerLineCounter;
int centerRedValue, centerGreenValue, centerBlueValue;

boolean centerLineBlack, leftLineBlack, rightLineBlack;

/*********************************************

  LEDS

********************************************/

int led1_pin = 5;
int led2_pin = 6;
int led3_pin = 9;

int led4_pin = 10;
int led5_pin = 11;
int led6_pin = 13;

int led_sensor_left = 9;
int led_sensor_center = 10;
int led_sensor_right = 11;

int led1, led2, led3;

int RGBstate = 1;



/*********************************************

  MICROPHONE

********************************************/

int microphone;

#define MAX_SAMPLES 25
int samples[MAX_SAMPLES];

unsigned long start_time;
unsigned long end_time;


/*********************************************

  BUZZER

********************************************/
int speakerPin = 11;

int length = 15; // the number of notes
char notes[] = "ccggaagffeeddc "; // a space represents a rest
int beats[] = { 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 2, 4 };
int tempo = 300;

/*********************************************

  LINE VARS

********************************************/


int activeColor;    //   1: red   2: green   3:blue   // 0: white  // 4: black

int maxSpeed = 100;


/*********************************************

  SETUP

********************************************/

void setup()
{

  Serial.begin(115200);
  Serial1.begin(9600);
  Serial.println("Le starting");
  
  pinMode(DEBUG_PIN, OUTPUT);
  
  //////////////////////
  // bumpers
  //////////////////////
  
  gyro.bumpers.begin( SENSOR_OFF,  SENSOR_PROXIMITY, SENSOR_OFF);

  // Bumpers Configuration
  gyro.bumpers.setTriggerBumper(100);
  
  //////////////////////
  // motoruino 2
  //////////////////////
  
  // Motoruino2 Configuration
  gyro.motoruino2.config_imu(true, true, true);
  gyro.motoruino2.config_motor(M1_SIGNAL, M2_SIGNAL);
  gyro.motoruino2.config_power();

  gyro.motoruino2.setSpeedPWM(0, 0);

//  if (gyro.motoruino2.calibrateGyro())
//    Serial.println("Calibrated OK");
//  else
//    Serial.println("Not Calibrated");

  gyro.motoruino2.calibrateAngle();

  // Reset Gyroscope
  gyro.motoruino2.resetGyro(true, true, true);

  // Maximum ADC Current Value allowed to motor
  gyro.motoruino2.startMotorTrigger(150); //30;

  //gyro.motoruino2.setSpeedPWM(255, 255);
  
  delay(500);
  
  //////////////////////
  // others
  //////////////////////
  
  pinMode(speakerPin, OUTPUT);
  
  pinMode(bt1, INPUT_PULLUP);
  
  // LEDs RGB
  pinMode(led1_pin, OUTPUT);
  pinMode(led2_pin, OUTPUT);
  pinMode(led3_pin, OUTPUT);
  pinMode(led4_pin, OUTPUT);
  pinMode(led5_pin, OUTPUT);
  pinMode(led6_pin, OUTPUT);
  
  digitalWrite(led1_pin, LOW);
  digitalWrite(led2_pin, LOW);
  digitalWrite(led3_pin, LOW);
  digitalWrite(led4_pin, LOW);
  digitalWrite(led5_pin, LOW);
  digitalWrite(led6_pin, LOW);
  
  pinMode(led_sensor_left, OUTPUT);
  pinMode(led_sensor_center, OUTPUT);
  pinMode(led_sensor_right, OUTPUT);
  digitalWrite(led_sensor_left, LOW);
  digitalWrite(led_sensor_center, LOW);
  digitalWrite(led_sensor_right, LOW);


  delay(1000);
  
  Serial.print("Started");
  
  gyro.bumpers.setRGB(0,0,0);

}

/*********************************************

  LOOP

********************************************/

_GestureState center_gesture = NONE;

float redRacio,greenRacio,blueRacio;




void loop()
{

	if (sensorsCheck_metro.check())
	{
	  lineSensorsCheck();
	}

  lineFollow();

/*
	if (print_metro.check())
	{

//
//		Serial.print(leftLineBlack);
//		Serial.print(centerLineBlack);
//		Serial.print(rightLineBlack);

		Serial.print(" L:"); Serial.print(leftLine);
		Serial.print(" R:"); Serial.print(rightLine);
		Serial.print(" A:"); Serial.print(centerAmbient);
//		Serial.print(" R:"); Serial.print(redRacio);

//		Serial.print(" G:"); Serial.print(greenRacio);
//		Serial.print(" B:"); Serial.print(blueRacio);
//
		Serial.println();
	}


*/

} 

/*********************************************

  MOTOR TESTE

********************************************/


void motorTeste()
{
  /**/
  gyro.motoruino2.setSpeedPWM(255, 255);
  
  delay(1000);
  
  gyro.motoruino2.setSpeedPWM(0, 0);
  
  delay(1000);
  
  gyro.motoruino2.setSpeedPWM(-255, -255);
  
  delay(1000);
  
    gyro.motoruino2.setSpeedPWM(0, 0);
  
  delay(1000);
}
 
 
 
 
 
 
 
 
 
 
 
 /*********************************************

  OTHER STUFF

********************************************/

 
 
 
 
 
 
 
 
 
/*
 if (sensorsCheck_metro.check() == 1)
  {
    sensorsCheck();
    lineFollow();
    
    sendToConsole();
    //sendToProcessing();
    //sendToSerial1();
    
  }
 
 */
  
  //playMelody();
  

  
  
  /*
  gyro.motoruino2.setSpeedPWM(255, 0);
  
  delay(1000);
  
  gyro.motoruino2.setSpeedPWM(0, 0);
  
  delay(1000);
  
  gyro.motoruino2.setSpeedPWM(-255, 0);
  
  delay(1000);
  
    gyro.motoruino2.setSpeedPWM(0, 0);
  
  delay(1000);
  /**/
  
/*
void colorLoop()
{
  if (RGBled_metro.check() == 1)
  {
    if (RGBstate == 1)
    {
      //Serial.println("white");
      gyro.bumpers.setRGB(0, 0, 0);
 
      RGBstate++;
      return;
    }
    else if (RGBstate == 2)
    {
      //Serial.println("black");
      gyro.bumpers.setRGB(255, 255, 255);
   
      RGBstate++;
      return;
    }
    
    else if (RGBstate == 3)
    {
      //Serial.println("red");
      gyro.bumpers.setRGB(0,255,255);

      RGBstate++;
      return;
    }
    else if (RGBstate == 4)
    {
      //Serial.println("grenn");
      gyro.bumpers.setRGB(255,0,255);
 
      RGBstate++;
      return;
    }
    else if (RGBstate == 5)
    {
      //Serial.println("blue");
      gyro.bumpers.setRGB(255, 255, 0);
 
      RGBstate=1;
      return;
    }
    else
    {
      Serial.println("unknown");
      return;
    }
   
  }


}

*/
