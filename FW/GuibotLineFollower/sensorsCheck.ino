void lineSensorsCheck()
{

	gyro.bumpers.getLeftLine((unsigned short *) &leftLine);
	gyro.bumpers.getRightLine((unsigned short *) &rightLine);
	gyro.bumpers.getCenterAmbient((unsigned short *) &centerAmbient);
	gyro.bumpers.getCenterRed((unsigned short *) &centerRed);
	gyro.bumpers.getCenterGreen((unsigned short *) &centerGreen);
	gyro.bumpers.getCenterBlue((unsigned short *) &centerBlue);


	if (centerAmbient > 19)
	{
		redRacio = (float)centerRed/centerAmbient;
		greenRacio = (float)centerGreen/centerAmbient;
		blueRacio = (float)centerBlue/centerAmbient;
	} else
	{
		redRacio = 0;greenRacio = 0;blueRacio = 0;
	}

}


void sensorsCheck()
{
    //Serial.print(count++); Serial.print('\t');

    gyro.motoruino2.getCurrentValue(&value);
    current = value;
    
    gyro.motoruino2.getBatteryValue(&value);
    battery = value *  66 / 1024;
    
    heading = gyro.motoruino2.getGyroHeading();
    
    leftBumper = gyro.bumpers.CheckLeftBumper();
    rightBumper = gyro.bumpers.CheckRightBumper();
    
    // left proximity
    
    gyro.bumpers.getLeftProximity((unsigned short *) &leftProx);
    leftDistance = leftProx;
    
    // center proximity

    gyro.bumpers.getCenterProximity((unsigned short *) &centerProx);
    centerDistance = centerProx;

    // right proximity
    
    gyro.bumpers.getRightProximity((unsigned short *) &rightProx);
    rightDistance = rightProx;
    
    // left light
    
    gyro.bumpers.getLeftAmbient((unsigned short *) &leftAmbient);
    leftAmbientLight = leftAmbient;
    
    // center light

    gyro.bumpers.getCenterAmbient((unsigned short *) &centerAmbient);
    centerAmbientLight = centerAmbient;

    // right light
    
    gyro.bumpers.getRightAmbient((unsigned short *) &rightAmbient);
    rightAmbientLight = rightAmbient;
    
    // left line
    
    gyro.bumpers.getLeftLine((unsigned short *) &leftLine);
    leftLineValue = leftLine;
    
    // right line
    
    gyro.bumpers.getRightLine((unsigned short *) &rightLine);
    rightLineValue = rightLine;
      
    // center rgb
      
    gyro.bumpers.getCenterRed((unsigned short *) &centerRed);
    centerRedValue = centerRed;
    
    gyro.bumpers.getCenterGreen((unsigned short *) &centerGreen);
    centerGreenValue = centerGreen;
    
    gyro.bumpers.getCenterBlue((unsigned short *) &centerBlue);
    centerBlueValue = centerBlue;

    centerLineValue = centerAmbient;
    

    
    
    
    
   
    /**/
    
    /*
    Serial.print(leftBumper);
    Serial.print(",");
    Serial.print(rightBumper);
    Serial.print(",");
    Serial.print(leftDistance);
    Serial.print(",");
    Serial.print(rightDistance);
    Serial.print(",");
    Serial.print(leftAmbient);
    Serial.print(",");
    Serial.print(rightAmbient);
    Serial.print(",");
    */
    
    /*
    
	gyro.bumpers.setRGB(0,255,255);

	gyro.bumpers.getLeftProximity((unsigned short *) &leftProx);	Serial.print(leftProx);	Serial.print('\t');
	gyro.bumpers.getLeftGesture((_GestureState *) &leftGesture);	Serial.print(leftGesture);	Serial.print('\t');
	gyro.bumpers.getLeftRed((unsigned short *) &leftRed);			Serial.print(leftRed);	Serial.print('\t');
	gyro.bumpers.getLeftGreen((unsigned short *) &leftGreen);		Serial.print(leftGreen);	Serial.print('\t');
	gyro.bumpers.getLeftBlue((unsigned short *) &leftBlue);			Serial.print(leftBlue);	Serial.print('\t');
	gyro.bumpers.getLeftAmbient((unsigned short *) &leftAmbient);	Serial.print(leftAmbient);	Serial.print('\t');
	gyro.bumpers.getLeftLine((unsigned short *) &leftLine);			Serial.print(leftLine);	Serial.print('\t');

	gyro.bumpers.setRGB(255,0,255);

	gyro.bumpers.getRightProximity((unsigned short *) &rightProx);	Serial.print(rightProx);	Serial.print('\t');
	gyro.bumpers.getRightGesture((_GestureState *) &rightGesture);	Serial.print(rightGesture);	Serial.print('\t');
	gyro.bumpers.getRightRed((unsigned short *) &rightRed);			Serial.print(rightRed);	Serial.print('\t');
	gyro.bumpers.getRightGreen((unsigned short *) &rightGreen);		Serial.print(rightGreen);	Serial.print('\t');
	gyro.bumpers.getRightBlue((unsigned short *) &rightBlue);			Serial.print(rightBlue);	Serial.print('\t');
	gyro.bumpers.getRightAmbient((unsigned short *) &rightAmbient);	Serial.print(rightAmbient);	Serial.print('\t');
	gyro.bumpers.getRightLine((unsigned short *) &rightLine);			Serial.print(rightLine);	Serial.print('\t');

	gyro.bumpers.setRGB(255,255,0);

	gyro.bumpers.getCenterProximity((unsigned short *) &centerProx);	Serial.print(centerProx);	Serial.print('\t');
	gyro.bumpers.getCenterGesture((_GestureState *) &centerGesture);	Serial.print(centerGesture);	Serial.print('\t');
	gyro.bumpers.getCenterRed((unsigned short *) &centerRed);			Serial.print(centerRed);	Serial.print('\t');
	gyro.bumpers.getCenterGreen((unsigned short *) &centerGreen);		Serial.print(centerGreen);	Serial.print('\t');
	gyro.bumpers.getCenterBlue((unsigned short *) &centerBlue);			Serial.print(centerBlue);	Serial.print('\t');
	gyro.bumpers.getCenterAmbient((unsigned short *) &centerAmbient);	Serial.print(centerAmbient);	Serial.print('\t');

    */

    
 
 }
