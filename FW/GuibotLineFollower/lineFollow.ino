// active color - read value

// activeColor;    //   1: red   2: green   3:blue   // 0: white  // 4: black

int red_r, red_g, red_b, red_l;
int green_r, green_g, green_b, green_l;
int blue_r, blue_g, blue_b, blue_l;
int white_r, white_g, white_b, white_l;
int black_r, black_g, black_b, black_l;

int leftSpeed, rightSpeed;

#define _LEFT_ 1
#define _RIGHT_ 2
#define _NULL_ 0

bool lastLeft, lastRight;
char lastDirection;

int reverse_speed = -25;
float speedFilter;
float speedFilter_fast = 0.9;
float speedFilter_slow = 0.55;

float leftSpeed_smooth, rightSpeed_smooth;

void lineFollow()
{

	lastLeft = leftLineBlack;
	lastRight = rightLineBlack;

	// Check all the sensors for black or white
	leftLineBlack = leftLine > 990;
	centerLineBlack = centerAmbient < 90;
	rightLineBlack = rightLine > 990;

	if ((lastLeft == 1) && (leftLineBlack == 0))
	{
		lastDirection = _LEFT_;
	}
	if ((lastRight == 1) && (rightLineBlack == 0))
	{
		lastDirection = _RIGHT_;
	}

	if ((leftLineBlack == 0) && (centerLineBlack == 1) && (rightLineBlack == 0))
	{
		lastDirection = _NULL_;
	}

	digitalWrite(led_sensor_left, leftLineBlack);
	digitalWrite(led_sensor_center, centerLineBlack);
	digitalWrite(led_sensor_right, rightLineBlack);


	// Line follow logic:

	//   left    center   right
	//
	//     0       0       0      -    stop
	//
	//     1       0       0      -    turn left
	//
	//     1       1       0      -    turn left
	//
	//     1       1       1      -    maintain movement
	//
	//     0       1       1      -    turn right
	//
	//     0       0       1      -    turn right
	//
	//     0       1       0      -    move forward



	// stop
	if (!leftLineBlack && !centerLineBlack && !rightLineBlack)
	{
	//	leftSpeed = 0;
	//	rightSpeed = 0;

		if (lastDirection == _LEFT_)
		{
			leftSpeed = 0;
			rightSpeed = maxSpeed-10;

		}

		if (lastDirection == _RIGHT_)
		{
			leftSpeed = maxSpeed-10;
			rightSpeed = 0;
		}

	}

	// turn left - fast (only the side sns)
	else if (leftLineBlack && !centerLineBlack && !rightLineBlack)
	{
                speedFilter = speedFilter_fast;
		leftSpeed = reverse_speed;
		rightSpeed = maxSpeed;
	}

	// turn left - slow (side and midle sns)
	else if (leftLineBlack && centerLineBlack && !rightLineBlack)
	{
                speedFilter = speedFilter_fast;
		leftSpeed = reverse_speed;
		rightSpeed = maxSpeed-10;
	}

	// forward
	else if (leftLineBlack && centerLineBlack && rightLineBlack)
	{
                speedFilter = speedFilter_slow;
		//leftSpeed = maxSpeed;
		//rightSpeed = maxSpeed;
	}

	// right - slow (side and midle)
	else if (!leftLineBlack && centerLineBlack && rightLineBlack)
	{
                speedFilter = speedFilter_slow;
		leftSpeed = maxSpeed-10;
		rightSpeed = reverse_speed;
	}

	// right - fast (side only)
	else if (!leftLineBlack && !centerLineBlack && rightLineBlack)
	{
                speedFilter = speedFilter_fast;
		leftSpeed = maxSpeed;
		rightSpeed = reverse_speed;
	}

	// forward
	else if (!leftLineBlack && centerLineBlack && !rightLineBlack)
	{
                speedFilter = speedFilter_fast;
		leftSpeed = maxSpeed;
		rightSpeed = maxSpeed;
	}


        
        leftSpeed_smooth = leftSpeed_smooth * (1.0-speedFilter) + leftSpeed * speedFilter;
        rightSpeed_smooth = rightSpeed_smooth * (1.0-speedFilter) + rightSpeed * speedFilter;

	gyro.motoruino2.setSpeedPWM((int)leftSpeed_smooth, (int)rightSpeed_smooth);

        Serial.print((int)leftSpeed_smooth);
        Serial.print("  ");
        Serial.print((int)rightSpeed_smooth);
        Serial.println();


}


