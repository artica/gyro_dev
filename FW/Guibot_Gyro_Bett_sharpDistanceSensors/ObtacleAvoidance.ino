void obstacleAvoidance()
{
  
  dist_front_left = analogRead(dist_front_left_pin) > 1000;
  dist_front_right = analogRead(dist_front_right_pin) > 1000;
  dist_rear_left = analogRead(dist_rear_left_pin) > 1000;
  dist_rear_right = analogRead(dist_rear_right_pin) > 1000;
  
  if (!dist_front_left)
  {
    rightSpeed = 100;
    leftSpeed = 0;
    
  }
  
  if (!dist_front_right)
  {
    rightSpeed = 0;
    leftSpeed = 100;
  }
  
  gyro.motoruino2.setSpeedPWM(leftSpeed, rightSpeed);
  
}


void obstacleAvoidance1()
{
  
  
  // avoid obstacles
  if (leftDistance < 100 && leftDistance > 75)
  {
    digitalWrite(ledRED_left, HIGH);
    digitalWrite(ledBLUE_left, LOW);
    rightSpeed = 0;
  }
  else if (leftDistance > 100 && leftDistance < 255)
  {
    digitalWrite(ledRED_left, HIGH);
    digitalWrite(ledBLUE_left, HIGH);
    rightSpeed = -150;
  }
  else
  {
    digitalWrite(ledRED_left, LOW);
    digitalWrite(ledBLUE_left, LOW);
    rightSpeed = 100;
  }
  
  
  if (rightDistance < 100 && rightDistance > 75)
  {
    digitalWrite(ledRED_right, HIGH);
    digitalWrite(ledBLUE_right, LOW);
    leftSpeed = 0;
  }
  else if (rightDistance > 100 && rightDistance < 255)
  {
    digitalWrite(ledRED_right, HIGH);
    digitalWrite(ledBLUE_right, HIGH);
    leftSpeed = -150;
  }
  else
  {
    digitalWrite(ledRED_right, LOW);
    digitalWrite(ledBLUE_right, LOW);
     leftSpeed = 100; 
  }
  
  /*
  if (accelX < -0.15)
  {
    digitalWrite(ledRED_right, LOW);
    digitalWrite(ledBLUE_right, LOW);
    
    digitalWrite(ledGREEN_left, HIGH);
    digitalWrite(ledGREEN_right, HIGH);
    
    
    while (goBackCounter < goBackCounter_max)
    {
      goBackCounter++;
      Serial.println(goBackCounter);
      gyro.motoruino2.setSpeedPWM(-100, -100);
    }
    
      goBackCounter = 0;
      digitalWrite(ledGREEN_left, LOW);
      digitalWrite(ledGREEN_right, LOW);

  }
  else
  {
    gyro.motoruino2.setSpeedPWM(leftSpeed, rightSpeed);
  }
  */
  
   gyro.motoruino2.setSpeedPWM(leftSpeed, rightSpeed);
  
}

