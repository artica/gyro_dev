/*
 Artica CC - http://artica.cc

 Gyro Bumper V0.1 - Serras and Tarquinio

 This Software is under The MIT License (MIT)

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

// Update States for "later" reply (to do not block the Wire Interrupt driven Comm)
#define _DEBUG_BUMPER_ false

#define CMD_DATA	1
#define SHORT_DATA 	2
#define LONG_DATA 	4
#define FLOAT_DATA 	4

#define _LEFT 0
#define _CENTER 1
#define _RIGHT 2

// ------------------------------------------------------------------------------
// PARSER: Received an I2C "wire" message from the master - CRC already tested - MUST BE FAST!
// ------------------------------------------------------------------------------
void CommParseCommand(unsigned short howMany) {

	readyFlagToReply = false;

	// REPEAT THE ORDER ON THE SEND BUFFER
	OrderRequestBumper = (enum _ReceivedOrderBumper) receivedBuffer[0];

	// Repeat command to SendBuffer
	sendBuffer[0] = OrderRequestBumper;

	if (_DEBUG_BUMPER_) {
		Serial.print("CMD:");
		Serial.println((unsigned char) OrderRequestBumper);
	}

	switch (OrderRequestBumper) {

	case RX_NONE:
		CommReplyLaterBumper = NO_RX;
		break;

	case CFG:
		CommReplyLaterBumper = REPLY_CFG;
		break;

	case GLRED:
		startComm();
		addShortData(redLight[_LEFT]);
		replyData(CMD_DATA + SHORT_DATA);
		break;

	case GLGREEN:
		startComm();
		addShortData(greenLight[_LEFT]);
		replyData(CMD_DATA + SHORT_DATA);
		break;

	case GLBLUE:
		startComm();
		addShortData(blueLight[_LEFT]);
		replyData(CMD_DATA + SHORT_DATA);
		break;

	case GLAMBIENT:
		startComm();
		addShortData(ambientLight[_LEFT]);
		replyData(CMD_DATA + SHORT_DATA);
		break;

	case GLPROXIMITY:
		startComm();
		addShortData(proximity[_LEFT]);
		replyData(CMD_DATA + SHORT_DATA);
		break;

	case GLGESTURE:
		startComm();
		addShortData(gesture[_LEFT]);
		replyData(CMD_DATA + SHORT_DATA);
		break;

	case GLLINE:
		startComm();
		addShortData(getLeftLine());
		replyData(CMD_DATA + SHORT_DATA);
		break;

	case GRRED:
		startComm();
		addShortData(redLight[_RIGHT]);
		replyData(CMD_DATA + SHORT_DATA);
		break;

	case GRGREEN:
		startComm();
		addShortData(greenLight[_RIGHT]);
		replyData(CMD_DATA + SHORT_DATA);
		break;

	case GRBLUE:
		startComm();
		addShortData(blueLight[_RIGHT]);
		replyData(CMD_DATA + SHORT_DATA);
		break;

	case GRAMBIENT:
		startComm();
		addShortData(ambientLight[_RIGHT]);
		replyData(CMD_DATA + SHORT_DATA);
		break;

	case GRPROXIMITY:
		startComm();
		addShortData(proximity[_RIGHT]);
		replyData(CMD_DATA + SHORT_DATA);
		break;

	case GRGESTURE:
		startComm();
		addShortData(gesture[_RIGHT]);
		replyData(CMD_DATA + SHORT_DATA);
		break;

	case GRLINE:
		startComm();
		addShortData(getRightLine());
		replyData(CMD_DATA + SHORT_DATA);
		break;

	case GCRED:
		startComm();
		addShortData(redLight[_CENTER]);
		replyData(CMD_DATA + SHORT_DATA);
		break;

	case GCGREEN:
		startComm();
		addShortData(greenLight[_CENTER]);
		replyData(CMD_DATA + SHORT_DATA);
		break;

	case GCBLUE:
		startComm();
		addShortData(blueLight[_CENTER]);
		replyData(CMD_DATA + SHORT_DATA);
		break;

	case GCAMBIENT:
		startComm();
		addShortData(ambientLight[_CENTER]);
		replyData(CMD_DATA + SHORT_DATA);
		break;

	case GCPROXIMITY:
		startComm();
		addShortData(proximity[_CENTER]);
		replyData(CMD_DATA + SHORT_DATA);
		break;

	case GCGESTURE:
		startComm();
		addShortData(gesture[_CENTER]);
		replyData(CMD_DATA + SHORT_DATA);
		break;

	case SRGB:
		analogWrite(PinR, (unsigned char) getCharData(1));
		analogWrite(PinG, (unsigned char) getCharData(2));
		analogWrite(PinB, (unsigned char) getCharData(3));
		break;

		// the shit just hit the fan
	default:
		Serial.println("Camandro!");
		break;

	}

}

