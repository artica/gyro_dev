/*
 Artica CC - http://artica.cc

 Gyro Bumper V0.1 - Serras and Tarquinio

 This Software is under The MIT License (MIT)

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

volatile unsigned char buffIndex = 0;

// ------------------------------------------------------------------------------
// Buffer prepare
// ------------------------------------------------------------------------------

void startComm() {
	buffIndex = 1;		// Take Command already filled into account
	sendBuffer[1] = 0;	// DUMMY
}

void addCharData(char reply) {
	sendBuffer[buffIndex] = (char) reply;

	buffIndex = buffIndex + 1;
}

void addShortData(short data) {
	for (unsigned char i = buffIndex; i < buffIndex + 2; i++)
		sendBuffer[i] = 0x00FF & (data >> ((i - buffIndex) * 8));

	buffIndex = buffIndex + 2;
}

void addLongData(long data) {
	for (unsigned char i = buffIndex; i < buffIndex + 4; i++)
		sendBuffer[i] = 0x000000FF & (data >> ((i - buffIndex) * 8));

	buffIndex = buffIndex + 4;
}

void addFloatData(float data) {
	char * FloatData;
	FloatData = (char*) &data;

	for (unsigned char i = buffIndex; i < buffIndex + 4; i++)
		sendBuffer[i] = FloatData[i - buffIndex];

	buffIndex = buffIndex + 4;

}

// ------------------------------------------------------------------------------
// Activate Flag Reply
// ------------------------------------------------------------------------------
// ------------ Prepare the Reply with data --------------
void replyData(unsigned char bufferSize) {
	CommReplyLaterBumper = NO_RX;
	sendLength = bufferSize;					// Buffer Size
	readyFlagToReply = true;
}
// ------------ Prepare the Reply as true --------------
void replyTrue(void) {
	sendBuffer[1] = TRUE;
	sendLength = 1 + 1;				// Buffer Size = 1 (cmd) + 1 (data = TRUE)
	CommReplyLaterBumper = NO_RX;
	readyFlagToReply = true;
}
// ------------ Prepare the Reply as false --------------
void replyFalse(void) {
	sendBuffer[1] = FALSE;
	sendLength = 1 + 1;				// Buffer Size = (cmd) + 1 (data = FALSE)
	CommReplyLaterBumper = NO_RX;
	readyFlagToReply = true;
}

// ------------------------------------------------------------------------------
// Pooling event to REPLY ORDERS updated - can be interrupted
// ------------------------------------------------------------------------------
void UpdateComm() {
	// REPLY TO MASTER (the received "Wire" Orders)
	switch (CommReplyLaterBumper) {

	// Dummy state
	case NO_REPLY:
		break;

	// Config State
	case REPLY_CFG:

		/* TODO: implement config selections for power management */
		break;

	// The shit just hit the fan
	default:
		break;
	};
}

// BUFFER AUX FUNCTIONS
char getCharData(unsigned char index) {
	return receivedBuffer[index];
}

short getShortData(unsigned char index) {
	short Data = 0;
	char *Padding = (char*) &Data;

	for (unsigned char i = index; i < index + 2; i++) {
		Padding[i - index] = ((char) receivedBuffer[i]);
	}
	return Data;
}

long getLongData(unsigned char index) {
	long Data = 0;
	char *Padding = (char*) &Data;

	for (unsigned char i = index; i < index + 4; i++) {
		Padding[i - index] = ((char) receivedBuffer[i]);
	}
	return Data;
}

float getFloatData(unsigned char index) {
	float reply = 0;

	char *Padding = (char*) &reply;

	for (unsigned char i = index; i < index + 4; i++) {
		Padding[i - index] = ((char) receivedBuffer[i]);
	}
	return reply;
}

