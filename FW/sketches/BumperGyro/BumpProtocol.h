/*
 Artica CC - http://artica.cc

 Gyro Bumper V0.1 - Serras and Tarquinio

 This Software is under The MIT License (MIT)

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */


#ifndef _BUMPERPROTOCOL_H_
#define _BUMPERPROTOCOL_H_

// --------------------------------------------------------------------------------
// PROTOCOL Commands Received - I2C/TWI - Interrupt Managed (decoded on Parser function)
enum _ReceivedOrderBumper {
	RX_NONE,				// DEFAULT....
	CFG,						// CONFIG ALL

	GLGESTURE,
	GLRED,
	GLGREEN,
	GLBLUE,
	GLAMBIENT,
	GLPROXIMITY,
	GLLINE,

	GRGESTURE,
	GRRED,
	GRGREEN,
	GRBLUE,
	GRAMBIENT,
	GRPROXIMITY,
	GRLINE,

	GCGESTURE,
	GCRED,
	GCGREEN,
	GCBLUE,
	GCAMBIENT,
	GCPROXIMITY,

	SRGB						// Set RGB Value
};

// --------------------------------------------------------------------------------
// Update States for "later" reply (to do not block the Wire Interrupt for too much time)
enum _ProcessLaterOrdersBumper {
	NO_RX, REPLY_CFG						// CONFIG ALL

};

enum _GestureState {
	UP, DOWN, LEFT, RIGHT, NEAR, FAR, NONE
};

#endif /* _BUMPERPROTOCOL_H_ */
