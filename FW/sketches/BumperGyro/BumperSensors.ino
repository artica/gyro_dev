/*
 Artica CC - http://artica.cc

 Gyro Bumper V0.1 - Serras and Tarquinio

 This Software is under The MIT License (MIT)

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

void InitializeSensors(void) {
	// Initialize APDS-9960 (configure I2C and initial values)
	for (int count = 0; count < 3; count++) {
		if (apds[count].init(pins[count * 2], pins[count * 2 + 1])) {
			Serial.print("APDS-9960 ");
			Serial.print(words[count]);
			Serial.println(" initialization complete");
		} else {
			Serial.print("Something went wrong during");
			Serial.print(words[count]);
			Serial.println(" APDS-9960 init!");
		}

		// Start running the APDS-9960 light sensor (no interrupts)
		if (apds[count].enableLightSensor(false)) {
			Serial.print(words[count]);
			Serial.println("Light sensor is now running");
		}

		// Adjust the Proximity sensor gain
		if (!apds[count].setProximityGain(PGAIN_8X)) {
			Serial.println("Something went wrong trying to set PGAIN");
		}

		// Start running the APDS-9960 proximity sensor engine
		if (!apds[count].enableProximitySensor(false)) {
			Serial.print("Failed Proximity");
		}

		//		// Start running the APDS-9960 gesture sensor engine
		//		if ( !apds[count].enableGestureSensor(false) ) {
		//			Serial.println(F("Gesture failed"));
		//		}

	}
}

void readProximity(int counter) {

	apds[counter].readProximity(proximity[counter]);

}

void readGesture(int counter) {

	//    // Start running the APDS-9960 gesture sensor engine
	//	if ( !apds[counter].enableGestureSensor(false) ) {
	//		Serial.println(F("Gesture failed"));
	//	}
	//
	//	UpdateComm();
	//
	//	// Read Gesture
	//	if ( apds[counter].isGestureAvailable() ) {
	//	  gesture[counter] = apds[counter].readGesture();
	//	}

	//	UpdateComm();
	//
	//	// Read Gesture
	//	if ( apds[counter].isGestureAvailable() ) {
	//		gesture[counter] = apds[counter].readGesture();
	//	}

	gesture[counter] = 0;

}

void TurnOffSensors(void) {
	for (int count = 0; count < 3; count++) {
		apds[count].disableLightSensor();
		apds[count].disablePower();
	}
}

unsigned short getRightLine() {
	unsigned short value = 0;

	digitalWrite(PinLinePWR, LOW);
	delayMicroseconds(500);
	value = analogRead(PinLineRight);
	digitalWrite(PinLinePWR, HIGH);

	return value;
}

unsigned short getLeftLine() {
	unsigned short value = 0;

	digitalWrite(PinLinePWR, LOW);
	delayMicroseconds(500);
	value = analogRead(PinLineLeft);
	digitalWrite(PinLinePWR, HIGH);

	return value;
}

