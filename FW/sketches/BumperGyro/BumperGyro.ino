/*
 Artica CC - http://artica.cc

 Gyro Bumper V0.1 - Serras and Tarquinio

 v0.0 01/11/2015 - PCB without uC and without Color Sensor
 v0.1 23/11/2015 - PCB with uC (ICSP with wrong pinout!! and I2C HW SDA and SCL pins bad labeled)

 The APDS9960 code is based on the Sparkfun example code, but with integration of the SoftwareWire lib

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!! PROGRAM ONLY WITH 3V3 FTDI !!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!! Arduino Pro Mini 3.3V 8MHz !!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 This Software is under The MIT License (MIT)

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

#include <Arduino.h>
#include <avr/interrupt.h>
#include <SoftwareWire.h>
#include <Metro.h>
#include "SparkFun_APDS9960_SoftwareWire.h"
#include "BumpProtocol.h"
#include "Motoruino2Comm.h"
#include "Wire.h"

#define PinR 10
#define PinG 13
#define PinB 9

#define PinLeft 	A3			// Bumper Switch Left (not in use)
#define PinRight 	A2			// Bumper Switch Right (not in use)

#define PinLineLeft 	A0			// Line Follower Left
#define PinLineRight 	A1			// Line Follower Right
#define PinLinePWR 		8			// Line Follower Power

#define PinSDA 		A4
#define PinSCL 		A5

#define PinSDALeft 	7
#define PinSCLLeft 	6

#define PinSDACenter 	5
#define PinSCLCenter   	4

#define PinSDARight 	3
#define PinSCLRight 	2

#define PinADC 	A6

#define BUFFER_SIZE 	10			// I2C Buffer size
#define BUMPER_ADDRESS 	4			// Slave I2C address

#define SENSOR_READ_RATE 50

enum _ReceivedOrderBumper OrderRequestBumper = RX_NONE;
enum _ProcessLaterOrdersBumper CommReplyLaterBumper = NO_RX;
enum _GestureState GestureState = NONE;

// Wire Comm
char receivedBuffer[BUFFER_SIZE];
char sendBuffer[BUFFER_SIZE];
unsigned char sendLength = 0;

Metro MetroBlinkTime = Metro(SENSOR_READ_RATE);

bool readyFlagToReply = true;

SparkFun_APDS9960_SoftwareWire apds[3] = { SparkFun_APDS9960_SoftwareWire(),
		SparkFun_APDS9960_SoftwareWire(), SparkFun_APDS9960_SoftwareWire() };

unsigned char pins[6] = { 7, 6, 5, 4, 3, 2 };

char const * words[3] = { "Left", "Center", "Right" };

uint16_t ambientLight[3];
uint16_t redLight[3];
uint16_t greenLight[3];
uint16_t blueLight[3];
uint16_t gesture[3];
uint8_t proximity[3];

unsigned int anaL, anaR;

bool leftProximity = true;
bool leftRGB = true;
bool leftGesture = true;

bool centerProximity = true;
bool centerRGB = true;
bool centerGesture = true;

bool rightProximity = true;
bool rightRGB = true;
bool rightGesture = true;

bool leftLine = true;
bool RightLine = true;

// the setup function runs once when you press reset or power the board
void setup() {

	Serial.begin(115200);

	pinMode(PinR, OUTPUT);
	pinMode(PinG, OUTPUT);
	pinMode(PinB, OUTPUT);

	pinMode(PinLeft, INPUT_PULLUP);
	pinMode(PinRight, INPUT_PULLUP);

	pinMode(PinLineLeft, INPUT_PULLUP);
	pinMode(PinLineRight, INPUT_PULLUP);

	pinMode(PinLinePWR, OUTPUT);
	digitalWrite(PinLinePWR, LOW);

	pinMode(PinADC, INPUT_PULLUP);

	digitalWrite(PinR, HIGH);
	digitalWrite(PinG, HIGH);
	digitalWrite(PinB, HIGH);

	InitializeSensors();

	commConfig();

}

unsigned long start = 0;

// the loop function runs over and over again forever
void loop() {

	UpdateComm();

	if (MetroBlinkTime.check() == 1) {


		for (int count = 0; count < 3; count++) {


			apds[count].readAmbientLight(ambientLight[count]);
			UpdateComm();
			apds[count].readRedLight(redLight[count]);
			UpdateComm();
			apds[count].readGreenLight(greenLight[count]);
			UpdateComm();
			apds[count].readBlueLight(blueLight[count]);
			UpdateComm();

			readProximity(count);

			UpdateComm();

			readGesture(count);

			UpdateComm();

			//			  Serial.print(words[count]);
			//			  Serial.print("\tR:");
			//			  Serial.print(ambientRedLight[count]);
			//			  Serial.print("\tG:");
			//			  Serial.print(ambientGreenLight[count]);
			//			  Serial.print("\tB:");
			//			  Serial.print(ambientBlueLight[count]);
			//			  Serial.print("\tA:");
			//			  Serial.print(ambientLight[count]);
			//			  Serial.print("\tP:");
			//			  Serial.print(proximity[count]);
			//			  Serial.println("\t");

		}


		//Serial.println();
	}

}

