/*
 Artica CC - http://artica.cc

 -- > guibot @ BETT-UK, Jan 2016   < --
 
 Gyro Bett has Obstacle Avoidance, Follow Light (WIP) and also Gyro Mobile (by Tarquinio and Serras).
 
 Devices used in this code:
 
 2 RGB LEDs on pins 13, 12, 11, 10, 9, 8, 7
 1 Button on pin 7
 1 Buzzer on pin 5
 
 The button must be pressed to change the state.
 
 -- >     < --

 This App while on the Mobile Functions is to integrate with mobile App developed to Android platform using
 the Bluetooth HC-05 Xbee Module on Motoruino2 platform

 -- >     < --

 This Software is under The MIT License (MIT)

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

#include <Gyro.h>
#include <Motoruino2.h>
#include <GyroSerialCommand.h>
#include <Wire.h>
#include <Metro.h>
#include <SPI.h>
#include <Servo.h>  

// Define the rotation (chose between 1 or -1)
#define M1_SIGNAL -1
#define M2_SIGNAL 1
#define DEBUG_PIN 13
int debug = 0;

Gyro gyro;
GyroSerialCommand serial_command;    // Command interpreter

unsigned long last_update = 0;
int command_timeout = 250;
Metro log_metro = Metro(50);

/*********************************************

  DEBOUNCER VARS

********************************************/

// Variables will change:
int ledState = HIGH;         // the current state of the output pin
int buttonState;             // the current reading from the input pin
int lastButtonState = LOW;   // the previous reading from the input pin

// the following variables are long's because the time, measured in miliseconds,
// will quickly become a bigger number than can be stored in an int.
long lastDebounceTime = 0;  // the last time the output pin was toggled
long debounceDelay = 50;    // the debounce time; increase if the output flickers

/*********************************************

  LEDS and buttonPinS

********************************************/

Metro led_blink_metro = Metro (500);
int ledRED_left = 11;
int ledBLUE_left = 13;
int ledGREEN_left = 12;

int ledRED_right = 8;
int ledBLUE_right = 10;
int ledGREEN_right = 9;

int RGBstate = 1;

int ledsRGB[6] = {
  ledRED_right,
  ledGREEN_right,
  ledBLUE_right,
  ledRED_left,
  ledGREEN_left,
  ledBLUE_left
};

int buttonPin = 7;
int debounceCounter = 0;

/*********************************************

  SENSORS VARS

********************************************/
unsigned short leftProx, leftRed, leftGreen, leftBlue, leftAmbient, leftLine, leftGesture;
unsigned short rightProx, rightRed, rightGreen, rightBlue, rightAmbient, rightLine, rightGesture;

unsigned int leftDistance, rightDistance, leftLight, rightLight;

/*********************************************

  NAVIGATION CONTROL VARS

********************************************/

int goBackCounter;
int goBackCounter_max = 100;
int leftSpeed, rightSpeed;
float leftSpeed_smooth, rightSpeed_smooth;



/*********************************************

  BUZZER

********************************************/
int buzzerPin = 5;

int length = 15; // the number of notes
char notes[] = "ccggaagffeeddc "; // a space represents a rest
int beats[] = { 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 2, 4 };
int tempo = 300;

/*********************************************

  SETUP

********************************************/


void setup() 
{

    Serial.begin(9600);
    Serial1.begin(9600);

    Serial.println(F("Le starting"));    
    serial_command.addCommand("diff",  commandDifferential);
    serial_command.addCommand("traj",  commandTrajectory);
    serial_command.addCommand("tone",  commandTone);
    pinMode(DEBUG_PIN, OUTPUT); 
 
    for (int i=0; i<6; i++) 
      pinMode(ledsRGB[i], OUTPUT);
    
    pinMode(buttonPin, INPUT_PULLUP);
    
    beep(5, 10);
    delay(50);
    
    
    
    gyro.bumpers.begin( SENSOR_PROXIMITY,  SENSOR_PROXIMITY, SENSOR_PROXIMITY);
    
    Serial.println(F("Le Bumper"));  
    beep(5, 10);

    // Bumpers Configuration
    gyro.bumpers.setTriggerBumper(100);

	// Motoruino2 Configuration
	gyro.motoruino2.config_imu(true, true, true);
	gyro.motoruino2.config_motor(M1_SIGNAL,M2_SIGNAL);
	gyro.motoruino2.config_power();

	gyro.motoruino2.setSpeedPWM(0,0);

	gyro.motoruino2.startEncoder(Motoruino2Motor::FAST_ENCODER, 300, 50.0);

	// Current Limit for Motor
	gyro.motoruino2.startMotorTrigger(60);

	gyro.motoruino2.resetDistance();

    Serial.println(F("le imu"));  
    beep(5, 10);

    if (gyro.motoruino2.calibrateGyro())
        Serial1.println("Calibrated OK");
    else
        Serial1.println("Not Calibrated");

    gyro.motoruino2.resetGyro(true,true,true);
    
    beep(5, 10);
    Serial.println("finished calibrations");
    
    for (int i=0; i<10; i++)
    {
      ledson();
      delay(100);
      ledsoff();
      delay(100);
    }
    
    ledsoff();
    
    beep(5, 10);
    delay(500);
    beep(5, 10);
    delay(500);
    beep(5, 10);
    
    Serial.println("Started");

}

/*********************************************

  LOOP

********************************************/
int totalStates = 4;
int currentState; 

void loop()
{
  
  debounce();
  
  currentState = debounceCounter % 3;
  //Serial.println(currentState);
  
  switch (currentState)
  {
    case 0:
      obstacleAvoidance();
      blinkLEDS_RED();
    break;
    
    case 1:
      MobileControl();
      blinkLEDS();
    break;
    
    case 2:
      followLight();
    break;
    
    default:
      Serial.println("default");
  }  
  
  
  sensorsCheck();
  sensorsFeedBack();
  
  Serial.print(currentState);
  Serial.print("  ||  ");
  
  Serial.print(leftLight);
  Serial.print("   ");
  Serial.print(rightLight);
  Serial.print("  ||  ");
  
  Serial.print(leftDistance);
  Serial.print("   ");
  Serial.print(rightDistance);
 
  Serial.print("  ||  ");
  Serial.print(leftSpeed);
  Serial.print("   ");
  Serial.print(rightSpeed);
  
  Serial.print("  ||  ");
  Serial.print((int)leftSpeed_smooth);
  Serial.print("   ");
  Serial.println((int)rightSpeed_smooth);
  
  
  
  Serial.println();
}  

  



/*********************************************

  OTHERS

********************************************/


void ledson()
{
  for (int i = 0; i > 6; i++)
    digitalWrite(ledsRGB[i], HIGH);
}

void ledsoff()
{
  for (int i = 0; i > 6; i++)
    digitalWrite(ledsRGB[i], LOW);
}

/*********************************************

  SENSORS CHECK

********************************************/


void sensorsCheck()
{
  gyro.bumpers.getLeftProximity((unsigned short *) &leftProx);
  leftDistance = leftProx;
  
  gyro.bumpers.getRightProximity((unsigned short *) &rightProx);
  rightDistance = rightProx;
  
  gyro.bumpers.getLeftAmbient((unsigned short *) &leftAmbient);
  leftLight = leftAmbient;
  
  gyro.bumpers.getRightAmbient((unsigned short *) &rightAmbient);
  rightLight = rightAmbient;
  
}

/*********************************************

  SENSORS FEEDBACK

********************************************/

int beepTime;
int soundThreshold = 50;

void sensorsFeedBack()
{
  if (leftDistance > soundThreshold)
  {
      beepTime = map(leftDistance, 40, 255, 50, 10);
      beep(beepTime, 63);
  }
  
  if (rightDistance > soundThreshold)
  {
      beepTime = map(rightDistance, 40, 255, 50, 10);
      beep(beepTime, 229);
  }
  
  
}

/*********************************************

  BEEP

********************************************/

void beep(int delayms, int toner){
  analogWrite(buzzerPin, toner);      // Almost any value can be used except 0 and 255
                           // experiment to get the best tone
  delay(delayms);          // wait for a delayms ms
  analogWrite(buzzerPin, 0);       // 0 turns it off
  delay(delayms);          // wait for a delayms ms   
}  

/*********************************************

  BLINK LEDS

********************************************/
int theOtherLedState;

void blinkLEDS()
{
    if (led_blink_metro.check() == 1)
    {
      if (ledState == 255)
        ledState = 0;
      else
        ledState = 255;

      analogWrite(ledBLUE_left, ledState);
      
      if (ledState == 255)
        theOtherLedState = 0;
      else
        theOtherLedState = 255;
        analogWrite(ledBLUE_right, theOtherLedState);
    }
}



void blinkLEDS_RED()
{
    if (led_blink_metro.check() == 1)
    {
      if (ledState == 255)
        ledState = 0;
      else
        ledState = 255;

      analogWrite(ledRED_left, ledState);
      delay(25);
      digitalWrite(ledRED_left, LOW);
      
      
      if (ledState == 255)
        theOtherLedState = 0;
      else
        theOtherLedState = 255;
        
        analogWrite(ledRED_right, theOtherLedState);
        delay(25);
        digitalWrite(ledRED_right, LOW);
    }
}

