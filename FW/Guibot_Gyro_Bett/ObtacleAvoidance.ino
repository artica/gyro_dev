int maxSpeed = 200;
int reverseSpeed = -200;
int stopSpeed = 0;

float speedFilter_fast = 0.9;
float speedFilter_slow = 0.2;

float speedFilter = speedFilter_slow;

void obstacleAvoidance()
{
  
  
  
  // 
  if (leftDistance < 100 && leftDistance > 35)
  {
    digitalWrite(ledBLUE_left, HIGH);
    speedFilter = speedFilter_slow;
    rightSpeed = stopSpeed;
  }
  else if (leftDistance > 100 && leftDistance <= 255)
  {
    digitalWrite(ledBLUE_left, HIGH);
    speedFilter = speedFilter_fast;
    rightSpeed = reverseSpeed;
  }
  else
  {
    digitalWrite(ledBLUE_left, LOW);
    speedFilter = speedFilter_slow;    
    rightSpeed = maxSpeed;
  }
  
  
  if (rightDistance < 100 && rightDistance > 35)
  {
    digitalWrite(ledBLUE_right, HIGH);
    speedFilter = speedFilter_slow;
    leftSpeed = stopSpeed;
  }
  else if (rightDistance > 100 && rightDistance <= 255)
  {
    digitalWrite(ledBLUE_right, HIGH);  
    speedFilter = speedFilter_fast;  
    leftSpeed = reverseSpeed;
  }
  else
  {
    digitalWrite(ledBLUE_right, LOW);
    speedFilter = speedFilter_slow;
     leftSpeed = maxSpeed; 
  }
  
  /*
  if (accelX < -0.15)
  {
    digitalWrite(ledRED_right, LOW);
    digitalWrite(ledBLUE_right, LOW);
    
    digitalWrite(ledGREEN_left, HIGH);
    digitalWrite(ledGREEN_right, HIGH);
    
    
    while (goBackCounter < goBackCounter_max)
    {
      goBackCounter++;
      Serial.println(goBackCounter);
      gyro.motoruino2.setSpeedPWM(-100, -100);
    }
    
      goBackCounter = 0;
      digitalWrite(ledGREEN_left, LOW);
      digitalWrite(ledGREEN_right, LOW);

  }
  else
  {
    gyro.motoruino2.setSpeedPWM(leftSpeed, rightSpeed);
  }
  */
  
   //gyro.motoruino2.setSpeedPWM(leftSpeed, rightSpeed);
   
   leftSpeed_smooth = leftSpeed_smooth * (1.0-speedFilter) + leftSpeed * speedFilter;
   rightSpeed_smooth = rightSpeed_smooth * (1.0-speedFilter) + rightSpeed * speedFilter;

   gyro.motoruino2.setSpeedPWM((int)leftSpeed_smooth, (int)rightSpeed_smooth);
  
}

