int lightSpeedInc = 15;


void followLight()
{
  
  //leftLight
  
  if (leftLight > rightLight)
  {
    
    if (rightSpeed < 255)
      rightSpeed += lightSpeedInc;
    
    if (leftSpeed > 80)
      leftSpeed -= lightSpeedInc;
  
  }
  else
  {
    if (rightSpeed > 80)
      rightSpeed -= lightSpeedInc;
    
    if (leftSpeed < 255)
      leftSpeed += lightSpeedInc;
  }
  
  gyro.motoruino2.setSpeedPWM(leftSpeed, rightSpeed);
  
  
}
