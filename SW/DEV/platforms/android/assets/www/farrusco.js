// Prefix string for the bluetootn names that are considered to be Gyros
// The app can connect to any bluetooth serial port, this is just graphical
var GYRO_NAME_PREFIX = "Gyro";

// Minimum threshold distance (in pixels) to mark different points in a trajectory
var TRAJECTORY_MINIMUM_THRESHOLD = 20;

// Maximum number of points on a trajectory
var TRAJECTORY_MAXIMUM_POINTS = 80;

// Maximum offset the analog controller can move from the center of the base
// This is calculated in direct relation with the base size
var ANALOG_MAX_OFFSET = 1.6;

// Number of the bluetooth device to connect to. This is the position 
// of the device on the list of bluetooth devices, NOT the device address
var connect_device_target = 0;

// Mac address of the bluetooth device currently connected
var connect_device_address = 0;

// Dlay between each command re-transmission when pressing the movement controls
var REPEAT_COMMAND_DELAY = 20;

// Base coordinates for the edge of the analog stick
var analog_stick_anchor_left;
var analog_stick_anchor_top;
// Current coordinates for the edge of the analog stick
var analog_stick_position_left;
var analog_stick_position_top;
// Maximum offset movement for the analog stick
var analog_stick_max_offset;
// Is the analog controller moving?
var moving_analog = false;

// Initial scale to use when drawing the trajectory
var scale_value = 1.0;
 
// Initial position for the neck of the Gyro (servo with the distance sensor)
var neck_pos = 100;

// Currently selected main tab
var selected_main_tab = '';

// Device physical orientation (0 = portrait, -90, 90 = landscape)
var device_orientation = 0;

// Context for the draw trajectory canvas
var trajectory_context;


// Association between the main tabs, their selector images and callback 
// functions used to create the content for each one
var MAIN_TAB =
[
	{ button:'img_select_connection', image:'connect', create:createWindowConnect},
	{ button:'img_control_direct', image:'control_direct', create:createWindowControl},
	{ button:'img_control_trajectory', image:'control_draw', create:createWindowTrajectory},
	{ button:'img_show_sensors', image:'show_sensors', create:createWindowSensors},
	{ button:'img_control_piano', image:'piano', create:createWindowPiano},
	{ button:'img_control_program', image:'control_program', create:createWindowProgram},
]

// Show a specific tab from a tab group, given a pressed image
function selectTab(tab_group, tab_button) 
{
	for (var i=0; i<tab_group.length; i++)
	{
		// Check if the button matches the tab being tested
		if (tab_group[i].button === tab_button)
		{
			// We got the right tab, let's fill the div with the correct content
			tab_group[i].create();
			// If it does, we need to show the ON image for the selector
			$('#'+tab_button).attr('src', 'images/'+tab_group[i].image+'_on.png');			
		}
		else
		{
			// If it doesn't, we need to show the OFF image for the selector
			$('#'+tab_group[i].button).attr('src', 'images/'+tab_group[i].image+'_off.png');
		}
	}
}

// Create the contents for the connection window
function createWindowConnect()
{
	console.log('createWindowConnect');
	var html = "<title>Escolha o seu Gyro da lista:</title>"+
			"<div id='list_devices'></div>"+			
			"<img id='img_refresh_devices' class='header_button' src='images/refresh_devices.png' />";

	// Create the page with the custom html
	$('#div_main_page').html(html).trigger("create");	
	
	// Connect tab callbacks	
	$('#img_refresh_devices').click(app.list);		
	app.list();
	
}

// Create the contents for the control window
function createWindowControl() 
{
	console.log('createWindowControl');
	// Check the current orientation of the device
	var portrait = window.orientation === 0;
	
	var html = '';
	// If the orientation is NOT portrait, the controls are side by side,
	// so we need to create an extra table with pre-set width spaces
	if (!portrait) html += "<table><tr><td style='width:50%'>";

	var orientation = portrait? 'portrait': 'landscape';

	// UP DOWN LEFT RIGHT keys
	html +=
		"<table id='movement_table'>"+
			"<tr>"+
				"<td></td>"+
				"<td><img id='move_forward' class='move_button move_button_"+orientation+
				"'src='images/move_front_off.png' /></td>"+
				"<td></td>"+
			"</tr>"+
			"<tr>"+
				"<td><img id='turn_left' class='move_button move_button_"+orientation+
				"' src='images/turn_left_off.png' /></td>"+
				"<td><img id='move_backward' class='move_button move_button_"+orientation+
				"' src='images/move_back_off.png'></td>"+
				"<td><img id='turn_right' class='move_button move_button_"+orientation+
				"' src='images/turn_right_off.png' /></td>"+
			"</tr>"+
		"</table>";
		
	if (!portrait) html += "</td><td style='width:50%'>";
		
	// Analog controller
	html +=	
		"<img id='analog_base' class='analog_base_"+orientation+
		"' src='images/joystick_ball_bg.png' />"+
		"<img id='analog_stick' class='analog_stick_"+orientation+
		"' src='images/joystick_ball_fw.png' />";

	if (!portrait) html += "</td></tr></table>";

	// Create the page with the custom html
	$('#div_main_page').html(html).trigger("create");	
	
	// We need to center the analog stick on top of the analog base
	// First we calculate the dimensions and positions of each element
	var base_width = $('#analog_base').width();
	var base_height = $('#analog_base').height();
	var base_left = $('#analog_base').offset().left;
	var base_top = $('#analog_base').offset().top;	
	var stick_width = $('#analog_stick').width();
	var stick_height = $('#analog_stick').height();
	
	//console.log('medidas: base_width:'+base_width+' base_height:'+base_height+' base_left:'+base_left+' base_top:'+base_top+' stick_width:'+stick_width+' stick_height:'+stick_height);
	
	// Now we set the corner position for the analog controller 
	// This position sets the controller exactly in the middle of the base
	analog_stick_anchor_left = analog_stick_position_left = Math.round(base_left+(base_width-stick_width)/2);
	analog_stick_anchor_top = analog_stick_position_top = Math.round(base_top+(base_height-stick_height)/2);
	
	// Puts the controller in the desired position
	$('#analog_stick').css('left', analog_stick_position_left);
	$('#analog_stick').css('top', analog_stick_position_top);

	// The maximum offset for the analog stick is proportional to the base size
	analog_stick_max_offset = base_width / ANALOG_MAX_OFFSET;
	
	// Direct control tab callbacks	
	$('.move_button').on( "mousedown touchstart", startDigitalControl );	
	$('.move_button').on( "mouseup touchend", endDigitalControl );	
	//$('.move_button').on( "mousemove touchmove", moveDigitalControl );

	$('#analog_stick').on( "mousedown touchstart", startAnalogControl );	
	$('#analog_stick').on( "mouseup touchend mouseleave", endAnalogControl );	
	$('#analog_stick').on( "mousemove touchmove", moveAnalogControl );
}

// Create the contents for the trajectory window
function createWindowTrajectory() 
{
	console.log('createWindowTrajectory');
	var html = 
		"<canvas id='canvas_trajectory' ></canvas>"+
		"<table>"+
			"<tr>"+
				"<td><img id='zoom_out' class='zoom_button' src='images/zoom_out.png' /></td>"+
				"<td style='width:100%'><input type='range' class='slider' name='slider-fill' "+
				"id='slider_scale' value='100' min='20' max='500' data-highlight='true'/></td>"+
				"<td><img id='zoom_in' class='zoom_button' src='images/zoom_in.png' /></td>"+
		"<table>"+
			"<tr>"+
				"<td><img id='send_trajectory' class='normal_button' src='images/icon_send.png'></td>"+
				"<td><img id='clear_trajectory' class='normal_button' src='images/icon_trash.png'></td>"+
			"</tr>"+
		"</table>";
	
	// Create the page with the custom html
	$('#div_main_page').html(html).trigger("create");	
	
	// Get the context for the trajectory canvas
	trajectory_context = $("#canvas_trajectory")[0].getContext('2d');

	var canvas_size = $('html').width() * 0.9;
	//var page_height = $('html').height();

	$('#canvas_trajectory').css({width:canvas_size, height:canvas_size});

	
	// Calculate the final width and height of the trajectory canvas
	//var width = $('#canvas_trajectory').width();
	//var height = $('#canvas_trajectory').height();

	// Make the canvas area equal to the canvas css size
	$('#canvas_trajectory').attr('width', canvas_size);
	$('#canvas_trajectory').attr('height', canvas_size);

	// Trajectory tab callbacks	
	$('#clear_trajectory').click(clearTrajectory);
	$('#send_trajectory').click(sendTraject);

	$('#canvas_trajectory').on( "mousedown touchstart", startTraject );	
	$('#canvas_trajectory').on( "mouseup touchend mouseleave", endTraject );
	$('#canvas_trajectory').on( "mousemove touchmove", moveTraject );

	$('#slider_scale').on( 'change', function(event) 
	{ 
		// If the canvas scale changes, we need to redraw everything
		scale_value = 100 / $(this).val(); 
		drawTrajectory();
	});
	// Redraw the trajectory canvas to show the grid
	drawTrajectory();
}

// Create the contents for the sensors window
function createWindowSensors() 
{
	console.log('createWindowSensors');
	var html = 
		"<h1>Collision sensors</h1>"+
		"<table id='sensor_bumpers_table'><tr>"+
			"<td><canvas id='sensor_bumper_left' class='linear_meter' ></canvas></td>"+
			"<td><canvas id='sensor_bumper_right' class='linear_meter' ></canvas></td>"+
		"</tr></table>"+
		"<h1>Proximity sensor</h1>"+
		"<canvas id='sensor_proximity' class='linear_meter' ></canvas>"+
		"<h1>Microphone</h1>"+
		"<canvas id='sensor_microphone' class='linear_meter' ></canvas>"+
		"<h1>Gyroscope</h1>"+
		"<canvas id='sensor_gyroscope' class='rotation_meter' ></canvas>";

	// Create the page with the custom html
	$('#div_main_page').html(html).trigger("create");	

	// Callback functions for when the sliders change
	$('#slider_neck').on( 'change', function(event) 
	{ 
		neck_pos = $(this).val(); 
	});	
	
	//setInterval(function () {fillRadialMeter( 'sensor_gyroscope', test_angle++);}, 20);			
}

//var test_angle = 0;

// Fills a linear meter (canvas) up to a certain value
// The value must be a normalized number [0, 1]
function fillLinearMeter( id, value)
{
	// Get the canvas and the matching context
	var canvas = $('#'+id)[0];
	var context = canvas.getContext("2d");	

	if (value > 1.0) value = 1.0;
	// Make the dimension of the canvas area equal to the css dimension
	var width = $('#'+id).width();
	var height = $('#'+id).height();
	$('#'+id).attr('width', width);
	$('#'+id).attr('height', height);

	// Clear the canvas area
	context.clearRect(0, 0, width, height);

	// If the value is positive, adjust it to the canvas dimensions
	if (value>0)
	{
		context.fillStyle = "#00A3D9";
		context.fillRect(3, 3, (width-6)*value, height-6);
		context.stroke();
	}
}

// Draws a pointer on a radial meter (canvas)
// The value can be any angle (in degrees)
function fillRadialMeter( id, angle)
{
	// Get the canvas and the matching context
	var canvas = $('#'+id)[0];
	var context = canvas.getContext("2d");	

	// Make the dimension of the canvas area equal to the css dimension
	var width = $('#'+id).width();
	var height = $('#'+id).height();
	$('#'+id).attr('width', width);
	$('#'+id).attr('height', height);

	// Clear the canvas area
	context.clearRect(0, 0, width, height);

	// Calculate the center of the canvas
	var center_x = width/2;
	var center_y = height/2;
	// Convert the angle to radians
	var alpha = angle*Math.PI/180;

	context.lineWidth = 10;	
	context.strokeStyle="#00A3D9";
	
	context.beginPath();
	context.moveTo(center_x, center_y);
	context.lineTo(	center_x+(center_x*0.9*Math.cos(alpha)), 
					center_y+(center_y*0.9*Math.sin(alpha)) );
	context.stroke();	
}

var MAX_PIANO_KEYS = 7;

// Create the contents for the piano window
function createWindowPiano() 
{
	console.log('createWindowPiano');
	// Create a table with all the major note keys
	var html = "<table id='piano_table'><tr>";
	for (var i=0; i<MAX_PIANO_KEYS; i++)
	{
		html += "<td><div id='piano_key_normal_"+i+"' class='piano_key_normal'></td>";
	}
	html += "</tr></table>";
	
	// Create floating divs with all the sharp keys
	for (var i=0; i<MAX_PIANO_KEYS; i++)
	{		
		// Ignore the notes that don't have sharp tones
		if ((i%7)!=2 && (i&7)!=6)
			html += "<div id='piano_key_sharp_"+i+"' class='piano_key_sharp'></div>";
	}

	// Create the page with the custom html
	$('#div_main_page').html(html).trigger("create");	

	// Reposition all the sharp keys on top of the major note keys
	for (var i=0; i<MAX_PIANO_KEYS-1; i++)
	{		
		// Ignore the notes that don't have sharp tones		
		if ((i%7)!=2 && (i&7)!=6)
		{
			var base_width_1 = $('#piano_key_normal_'+i).width();
			//var base_height = $('#piano_key_normal_'+i).height();
			var base_left_1 = $('#piano_key_normal_'+i).offset().left;
			var base_left_2 = $('#piano_key_normal_'+(i+1)).offset().left;
			var base_top = $('#piano_key_normal_'+i).offset().top;	

			console.log('i:'+i+'  base_top:'+base_top+'base_left_1:'+base_left_1+'base_left_2:'+base_left_2)

			var sharp_width = $('#piano_key_sharp_'+i).width();
			var sharp_height = $('#piano_key_sharp_'+i).height();

			// Puts the controller in the desired position
			$('#piano_key_sharp_'+i).css('left', base_left_2 - (base_left_2 - (base_left_1+base_width_1))/2 - sharp_width/2);
			$('#piano_key_sharp_'+i).css('top', base_top);
		}
	}

	// Piano tab callbacks	
	$('.piano_key_normal, .piano_key_sharp').on( "mousedown touchstart", playTone );	
	$('.piano_key_normal, .piano_key_sharp').on( "mouseup touchend mouseleave", stopTone );
}

function playTone(e)
{
	console.log('playTone');
	sendCommand("tone A", true); 
}

function stopTone(e)
{
	console.log('stopTone');
	sendCommand("tone 0", false); 
}

// Create the contents for the program window
function createWindowProgram() 
{
	console.log('createWindowProgram');
	var html = 
		"<div id='div_control_program' class ='main_block_type'>"+
			"<select>"+
				"<option value='1'>Seguir Paredes</option>"+
				"<option value='2'>Evitar Obstáculos</option>"+
			"</select>"+
			"<div id='div_control_program_follow_wall' class ='main_block_type'>"+
			"</div>"+
		"</div>";
		
	// Create the page with the custom html
	$('#div_main_page').html(html).trigger("create");			
}

// Callback for when a header button is clicked
function headerClicked(event)
{
	//console.log('headerClicked');
	// Get the id of the selector that was clicked
	var element = event.target || event.srcElement;	
	var id = $(element).attr('id');		
	// Show the desired tab and change the button image
	selected_main_tab = id;
	selectTab(MAIN_TAB, selected_main_tab);
}

$(document).ready(function()
{ 
	// Disables drags on the page so we can't drag elements around when it's not supposed
	window.ondragstart = function() { return false; }
	
	// Callback function to resize the elements if the window resizes
	$(window).resize(resizeWindow);
	
	// -----------------------------------------------
	// Header button callbacks
	$('.header_button').click(headerClicked);

	//Update the orientation of the device
	$(window).on('orientationchange', orientationChange);
});

// Callback function form when the device orientation changes 
// When the device is rotated between portrait and landscape mode
function orientationChange()
{
	console.log(window.orientation);
	selectTab(MAIN_TAB, selected_main_tab);
}

// Callback function for when the app is resized
function resizeWindow()
{
	console.log('resizeWindow');
	selectTab(MAIN_TAB, selected_main_tab);
    $('html').css('font-size',  $('body').width()/100);
	console.log($('body').width());
}


var app = 
{
    initialize: function() 
	{
        this.bind();
        resizeWindow();
	},
    bind: function() 
	{
        document.addEventListener('deviceready', this.deviceready, false);
    },
    deviceready: function() 
	{ 
		// Default tab when starting the application
		$('#img_select_connection').click();		
	},
	// Requests the list of known bluetooth devices
    list: function(event) 
	{
        if (typeof bluetoothSerial != "undefined") 
			bluetoothSerial.list(app.ondevicelist, app.generateFailureFunction("List Failed"));
    },
	// Requests a connection to a bluetooth device
    connect: function (e) 
	{
        console.log("app.connect");

        var elem = e.target;

        // Get the necessary variables from the HTML
        connect_device_target = null;

        while (connect_device_target === null)
        {
            connect_device_target = elem.getAttribute('device_number');
            connect_device_address = elem.getAttribute('device_address');

            // If we didn't fing the device propreties, check the parent element
            if (connect_device_target === null) elem = elem.parentNode;
        }

        console.log("connect_device_target:"+connect_device_target);


        app.connected_device = elem.getAttribute('device_number');;        

        //$('#connect_device_'+app.connected_device).css('background-color', '#FF7744');

        console.log("Requesting connection to " + connect_device_address);
		
		// Change the css so we show the "connection" icon next to the right button
		$('.connection_icon').css('display', 'none');
		$('#connection_icon_'+connect_device_target).attr('src', 'images/loading.gif');
		$('#connection_icon_'+connect_device_target).css('display', 'block');
		
        bluetoothSerial.connect(connect_device_address, app.onconnect, app.ondisconnect);        
    },
	// Requests the app to disconnect from the current bluetooth device
    disconnect: function(event)
	{
        if (event) { event.preventDefault();}
        if (typeof bluetoothSerial != "undefined") 
			bluetoothSerial.disconnect(app.ondisconnect);
    },
	// Callback triggered when the connection is established
    onconnect: function()
	{
		//console.log("Connected!!! WOHOO!!");

		$('#connection_icon_'+connect_device_target).attr('src', 'images/checked.png');
		$('#connection_icon_'+connect_device_target).css('display', 'block');
		
		bluetoothSerial.subscribe('\n', parseMessage, app.generateFailureFunction );	
    },
	// Callback triggered when the connection ends
    ondisconnect: function()
	{
		//console.log("Disconected!!!");
        console.log("Disconnected.");
		connect_device_target = 0;
		$('.connection_icon').css('display', 'none');
    },
    ondevicelist: function(devices) 
	{
		console.log("----- ondevicelist");
        var listItem, device_address;

        // remove existing devices
        list_devices.innerHTML = "";
        console.log("");
        
		var num = 1;
		var html = "<table>";

		devices.forEach(function(device) 
		{
            if (device.hasOwnProperty("uuid")) 
			{
                device_address = device.uuid;
            } 
			else if (device.hasOwnProperty("address")) 
			{
                device_address = device.address;
            } 
			else 
			{
                device_address = "ERROR " + JSON.stringify(device);
            }
			console.log('device:'+device.name+'  '+device_address);

			// Check if the bluetooth device starts with the name we want (gyro)
			var gyro = (device.name.indexOf(GYRO_NAME_PREFIX) === 0);
			
			html += "<tr><td style='width:80%'><div id='connect_device_"+num+
				"' class='device_item" + (gyro? " device_gyro": "" ) +"' "+
				"device_number='"+num+"' "+
				"device_address='"+device_address+"' >"+
				"<span class='device_name'>"+device.name+"</span><br/>"+
				"<span class='device_address'>" + device_address + "</span>"+
				"</div></td><td style='width:20%'>"+
				"<img id='connection_icon_"+num+"' class='connection_icon' "+
				"style='display:"+(device_address===connect_device_address? 'block': 'none')+"' "+
				"src='images/checked.png' /></td></tr>";
			num++;
        });
		html += "</table>";
		
		$('#list_devices').html(html);
		
		// Writes debug information about the devices found (or not :P)
        if (devices.length === 0) 
		{
            if (cordova.platformId === "ios") 
			{ // BLE
                console.log("No Bluetooth Peripherals Discovered.");
            } else 
			{ // Android
                console.log("Please Pair a Bluetooth Device.");
            }
        } 
		else 
		{
            console.log("Found " + devices.length + " device" + (devices.length === 1 ? "." : "s."));
        }

        $('.device_item').click(app.connect);      

    },
    generateFailureFunction: function(message) 
	{
        var func = function(reason) 
		{
            var details = "";
            if (reason) 
			{
                details += ": " + JSON.stringify(reason);
            }
            console.log(message + details);
        };
        return func;
    }
};

// Parses messages with incoming data from the arduino
function parseMessage(data)
{
	console.log("parseMessage:"+data);
	
	// Message with the current trajectory position
	if (data.indexOf('traj') === 0)
	{
		var tokens = data.split(' ');		
		current_trajectory_position = parseInt(tokens[1])+1;
		console.log("current_trajectory_position:"+current_trajectory_position);
		drawTrajectory();
	}
	// Message with Gyro sensor data
	else if (data.indexOf('sen') === 0 && selected_main_tab === 'img_show_sensors')
	{
		var tokens = data.split(' ');		
		fillLinearMeter('sensor_bumper_left', parseInt(tokens[1]));
		fillLinearMeter('sensor_bumper_right', parseInt(tokens[2]));
		fillLinearMeter('sensor_proximity', parseInt(tokens[3])/1024);
		fillLinearMeter('sensor_microphone', parseInt(tokens[4]));
		fillRadialMeter('sensor_gyroscope', - parseInt(tokens[5]));
	}
} 

function startDigitalControl(e)
{
	console.log('startDigitalControl');
	e.preventDefault();
	var element = event.target || event.srcElement;
	console.log(element.id);
	switch(element.id)
	{
		case 'move_forward': 
			$('#move_forward').attr('src', 'images/move_front_on.png'); 
			sendCommand("diff 255 255 100", true); 
		break;
		case 'move_backward': 
			$('#move_backward').attr('src', 'images/move_back_on.png'); 
			sendCommand("diff -255 -255 100", true);
		break;
		case 'turn_left':
			$('#turn_left').attr('src', 'images/turn_left_on.png');
			sendCommand("diff -255 255 100", true); 
		break;
		case 'turn_right': 
			$('#turn_right').attr('src', 'images/turn_right_on.png'); 
			sendCommand("diff 255 -255 100", true); 
		break;
	}
}

function endDigitalControl(e)
{
	e.preventDefault();
	var element = event.target || event.srcElement;
	//console.log('endDigitalControl:'+element.id);
	console.log('stop_movement');
	stopMovement();
}

// Starting position of the finger/mouse movement for the analog controller
var analog_stick_grab_left;
var analog_stick_grab_top;

function startAnalogControl(e)
{
	console.log('startAnalogControl');
	e.preventDefault();
	//var element = event.target || event.srcElement;
	//console.log(element.id);
	moving_analog = true;

	// Get the x, y coordinates, from both mobile devices and pc's
	var page_x = e.originalEvent.touches ? e.originalEvent.touches[0].pageX : e.pageX;
	var page_y = e.originalEvent.touches ? e.originalEvent.touches[0].pageY : e.pageY;

	analog_stick_grab_left = page_x;
	analog_stick_grab_top = page_y;

	console.log('analog_stick_grab_left:'+analog_stick_grab_left+'  analog_stick_grab_top:'+analog_stick_grab_top);
}

var TURN_ANGLE_FACTOR = 5;

function mapValues (value, fromSource, toSource, fromTarget, toTarget)
{
	return (value - fromSource) / (toSource - fromSource) * (toTarget - fromTarget) + fromTarget;
}

// Drag the analog control stick to a new position, triggered by a movement
function moveAnalogControl(e)
{
	e.preventDefault();
	
	if (!moving_analog) return;
	
	// Get the x, y coordinates, from both mobile devices and pc's
	var page_x = e.originalEvent.touches ? e.originalEvent.touches[0].pageX : e.pageX;
	var page_y = e.originalEvent.touches ? e.originalEvent.touches[0].pageY : e.pageY;
	
	// Calculate the offset since the beginning of the movement
	var left_offset = analog_stick_grab_left - page_x;
	var top_offset = analog_stick_grab_top - page_y;

	console.log('left_offset:'+left_offset+'  top_offset:'+top_offset);
	// Calculate the angle of the displacement and put
	var angle = Math.atan2(-(page_y-analog_stick_grab_top), page_x-analog_stick_grab_left);
	
	// Calculate the power of the movement (defined by the distance on the analog controller)
	var power = Math.sqrt( Math.pow(left_offset, 2) + Math.pow(top_offset, 2));
	power = Math.min( power, analog_stick_max_offset );
	
	// Check if the total offset if bigger/equal to the maximum value
	// If it is, the stick is placed at the maximum distance allowed 
	if ( power === analog_stick_max_offset )
	{
		analog_stick_position_left = Math.round(analog_stick_anchor_left + Math.cos(angle)*analog_stick_max_offset);
		analog_stick_position_top = Math.round(analog_stick_anchor_top - Math.sin(angle)*analog_stick_max_offset);
	}
	// Otherwise it's smaller, so we just draw it in the exact place relative to the click/touch
	else
	{
		analog_stick_position_left = analog_stick_anchor_left - left_offset;
		analog_stick_position_top = analog_stick_anchor_top - top_offset;
	}
	// Change the css to update the position
	$('#analog_stick').css({left: analog_stick_position_left, 
							top: analog_stick_position_top});	

	// The graphical stuff is done, now we need to calculate how all of this will
	//affect the speed of each motor on the Gyro
	
	// angle = 0 is now moving forward and always positive
	angle -= Math.PI/2;
	if (angle < - Math.PI) angle += 2 * Math.PI;
	
	// Convert the angle from radians to degrees
	angle =  Math.round( angle * 180 / Math.PI );

	console.log('angle:'+angle);
	var abs_angle = Math.abs(angle);

	var left_power = 255;
	var right_power = 255;
	
	if (abs_angle>90) 
	{
		left_power = -255;
		right_power = -255;
	}
		
	// If we're heading forward:
	if (abs_angle <= 45)
	{
		// heading forward, turning right
		if (angle<0) right_power = mapValues(abs_angle, 0, 45, 255, 0);
		// heading forward, turning left
		else left_power = mapValues(abs_angle, 0, 45, 255, 0);
	}
	// If we're turning left or right, with the control pointing forward
	else if (abs_angle> 45 && abs_angle <=90)
	{
		// rotating clockwise
		if (angle<0) right_power = mapValues(abs_angle, 45, 90, 0, -255);					
		// rotating counter-clockwise
		else left_power = mapValues(abs_angle, 45, 90, 0, -255);					
		
	}
	// If we're turning left or right, with the control pointing backward
	else if (abs_angle> 90 && abs_angle <135)
	{
		// rotating clockwise
		if (angle<0) left_power = mapValues(abs_angle, 90, 135, 255, 0);					
		// rotating counter-clockwise
		else right_power = mapValues(abs_angle, 90, 135, 255, 0);					
		
	}
	// The only option left is to be moving backward
	else
	{
		// heading backward, turning right
		if (angle<0) left_power = mapValues(abs_angle, 135, 180, 0, -255);	
		// heading forward, turning left
		else right_power = mapValues(abs_angle, 135, 180, 0, -255);	
	}
	
	// The calculated values for each motor are now adjusted with the power factor
	// which depends from the offset of the analog stick relative to the center
	var power_factor = power/analog_stick_max_offset;
	
	left_power = Math.round(left_power * power_factor);
	right_power = Math.round(right_power * power_factor);
	
	//console.log('angle:'+angle);
	sendCommand("diff "+left_power+" "+right_power+" 100", true);
}

function endAnalogControl(e)
{
	e.preventDefault();
	moving_analog = false;
	
	resetAnalogControl();
	stopMovement();
}

// Moves the analog controller back to the center position automatically
function resetAnalogControl()
{
	// Calculate the distance to move on each step (1/10 of the remaining distance)
	var move_left = (analog_stick_anchor_left - analog_stick_position_left)/10;
	var move_top = (analog_stick_anchor_top - analog_stick_position_top)/10;
	
	// Round the values up or down, depending on the movement direction
	move_left = move_left>0 ? Math.ceil(move_left) : Math.floor(move_left);
	move_top = move_top>0 ? Math.ceil(move_top) : Math.floor(move_top);

	//console.log('anchor:'+analog_stick_anchor_left+','+analog_stick_anchor_top+
	//			'  pos:'+analog_stick_position_left+','+analog_stick_position_top+
	//			'  move:'+move_left+','+move_top);
	
	// Change the position values and move the stick to the new position
	analog_stick_position_left += move_left;
	analog_stick_position_top += move_top;
	
	$('#analog_stick').css('left', analog_stick_position_left);
	$('#analog_stick').css('top', analog_stick_position_top);

	// If the stick didn't reach the center yet, schedule the next movement
	if (analog_stick_position_left != analog_stick_anchor_left ||
		analog_stick_position_top != analog_stick_anchor_top ) 
	setTimeout(resetAnalogControl, 20);
}


function stopMovement()
{
	$('#move_forward').attr('src', 'images/move_front_off.png');
	$('#move_backward').attr('src', 'images/move_back_off.png');
	$('#turn_left').attr('src', 'images/turn_left_off.png');
	$('#turn_right').attr('src', 'images/turn_right_off.png');
	sendCommand("diff 0 0 100", false); 
}

function transmitCommand(command)
{
	console.log('transmitCommand:'+command);
	if (typeof bluetoothSerial != "undefined") 
		bluetoothSerial.write(command+'\n');	
}

var command_timer;
var command_string;

function repeatCommand() 
{
    transmitCommand(command_string);
}

// Sends a command string using the currently active connection
// if repeat is true, the command is repeated every REPEAT_COMMAND_DELAY milis
function sendCommand(command, repeat)
{
	//console.log('sendCommand:'+command);	
	transmitCommand(command);

	// Clear any previous re-transmission there might be scheduled

	window.clearInterval(command_timer);
	// If the command is to be repeated, schedules the next transmission
	if (repeat)
	{
		// Saves the string to be re-transmitted
		command_string = command;
		command_timer = setInterval(function () {repeatCommand()}, REPEAT_COMMAND_DELAY);		
	}	
}

// Are we setting a trajectory or not? We start/continue setting a trajectory when 
// we click the trajectory canvas, and extend it by clicking on the the next point
// or by drawing it with the finger
var paint = false;


var trajectory_points = [];

var current_trajectory_position = 0;

function drawPoint(n)
{	
	//console.log("drawPoint:"+n);
	//trajectory_context.beginPath(); 
	//trajectory_context.moveTo(last_point.x, last_point.y);	
	
	trajectory_context.lineTo(trajectory_points[n].x*scale_value,
							  trajectory_points[n].y*scale_value);
}

// Draws a spot on a specific point of the trajectory, 
// using a specific fill and stroke colors
function drawSpot(pos, fill, stroke)
{
	// Draw the circle indicating the beginning of the path
	trajectory_context.beginPath();
	trajectory_context.arc(trajectory_points[pos].x*scale_value, 
		trajectory_points[pos].y*scale_value, 10, 0, 2 * Math.PI, false);
	trajectory_context.fillStyle = fill;
	trajectory_context.fill();
	
	trajectory_context.lineWidth = 5;	
	trajectory_context.strokeStyle = stroke;
	trajectory_context.stroke();		
}

// Draws part of the trajectory, starting and finishing on specific points
function drawPath(start, finish, stroke)
{
	console.log('drawPath('+start+', '+finish+', '+stroke+')');
	if (start>=finish) return;
	trajectory_context.lineWidth = 5;	
	trajectory_context.strokeStyle = stroke;
	trajectory_context.beginPath();
	trajectory_context.moveTo(trajectory_points[start].x*scale_value, 
						  trajectory_points[start].y*scale_value);
	
	for (var i=start+1; i<finish; i++)
	{
		drawPoint(i);
	}
	trajectory_context.stroke();

}
function drawTrajectory()
{
	console.log('drawTrajectory:'+ trajectory_points.length);
	console.log('scale_value:'+scale_value);
	
	var width = $('#canvas_trajectory').width();
	var height = $('#canvas_trajectory').height();

	trajectory_context.clearRect(0, 0, width, height);

	trajectory_context.lineWidth="1";
	trajectory_context.strokeStyle="gray";
	
	var grid_offset = 50*scale_value;
	
	for (var i=0; i<width; i+=grid_offset)
	{	
		trajectory_context.beginPath();
		trajectory_context.moveTo(i, 0);
		trajectory_context.lineTo(i, height);
		trajectory_context.stroke();	
	}
	for (var i=0; i<height; i+=grid_offset)
	{	
		trajectory_context.beginPath();
		trajectory_context.moveTo(0, i);
		trajectory_context.lineTo(width, i);
		trajectory_context.stroke();	
	}	

	// Nothing to draw if no points were set yet
	if (trajectory_points.length <= 0) return;

	if (trajectory_points.length >  1)
	{
		drawPath(0, current_trajectory_position, '#003300');

		drawPath(current_trajectory_position-1, trajectory_points.length, '#AA3300');
	}
	
	// Draw the spot indicating the beginning of the path
	drawSpot(0, '#44ee22', '#003300');		

	// If the the robot is currently moving, draw the spot at 
	// the current position on the trajectory
	if (current_trajectory_position < trajectory_points.length)
	{
		// Draw the circle indicating the beginning of the path
		drawSpot(current_trajectory_position, '#4444EE', '#003300');
	}
}

function addPoint(pos_x, pos_y)
{
	// Will not add more points if we're at maximum capacity already
	if (trajectory_points.length > TRAJECTORY_MAXIMUM_POINTS) return;
	
	// If the trajectory is empty, add the current point as the initial point
	if (trajectory_points.length == 0)
	{
		trajectory_points.push({x:pos_x/scale_value, y:pos_y/scale_value});
		current_trajectory_position = 1;
	}
	// Otherwise, let's see if the current point is far enough from the last point
	// so that it's relevant to store it
	else
	{
		// Get the distance between the last point in the trajectory and the current point
		var last_pos = trajectory_points[trajectory_points.length - 1];
		var current_pos = {x:pos_x/scale_value, y:pos_y/scale_value};
		var distance = calculateDistance(last_pos, current_pos);
		
		// The new point is only added if it's far enough from the last point
		if (distance > TRAJECTORY_MINIMUM_THRESHOLD/scale_value)
		{
			trajectory_points.push(current_pos);
			current_trajectory_position = trajectory_points.length;
		}
	}
	drawTrajectory();
}

function clearTrajectory()
{
	trajectory_points = [];
	current_trajectory_position = 0;
	drawTrajectory();
}

// Calculate the angle (in degrees) between two points
function calculateDistance(p1, p2)
{
	// Calculate the euclidean distance between two points
	return Math.round( Math.sqrt( Math.pow(p2.x - p1.x, 2) + Math.pow(p2.y - p1.y, 2) ) );
}

// Calculate the angle (in degrees) between two points
function calculateAngle(p1, p2)
{
	// Calculate the angle value
	var theta = Math.atan2(-(p2.y-p1.y), p2.x-p1.x);
	// Make sure the angle is positive
	if (theta < 0) theta += 2 * Math.PI;
	// Return the angle value in degrees 
	return Math.round( theta * 180 / Math.PI );
}

// Convert a number to a padded hexadecimal string with 3 digits
function toPaddedHex(value)
{
	return  ('000'+value.toString(16)).slice(-3);
	//return  ('000'+value).slice(-3);
}

function sendTraject()
{
	console.log('sendTraject');

	if (trajectory_points.length < 2) return;

	console.log('scale_value:'+scale_value);
	
	// The initial segment of the trajectory is considered to have the angle = 0
	var initial_angle = calculateAngle(trajectory_points[0], trajectory_points[1]);

	var trajectory = 'traj '+ toPaddedHex(trajectory_points.length-1) + ','+
		toPaddedHex(0)+';'+	toPaddedHex(calculateDistance(trajectory_points[1],
										trajectory_points[0]) / scale_value )+',';

	for (var i=2; i<trajectory_points.length; i++)
	{
		var angle =  calculateAngle(trajectory_points[i-1], trajectory_points[i]) - initial_angle;
		if (angle<0) angle += 360; 
		trajectory += toPaddedHex( angle )+';'+
			toPaddedHex( Math.round( calculateDistance(trajectory_points[i-1],
									 trajectory_points[i]) / scale_value  ) )+',';
	}
	console.log(trajectory);
	transmitCommand(trajectory+'\n');	
}

function startTraject(e)
{
	// Get the coordinates of the click relative to the canvas
    var parent_offset = $(this).parent().offset(); 
	var pos_x = (e.originalEvent.touches ? e.originalEvent.touches[0].pageX : e.pageX) - parent_offset.left;
	var pos_y = (e.originalEvent.touches ? e.originalEvent.touches[0].pageY : e.pageY) - parent_offset.top;

	addPoint(pos_x, pos_y);
	paint = true;
}

function moveTraject(e)
{
	e.preventDefault();
    var parent_offset = $(this).parent().offset(); 
	
	// Get the x, y coordinates, from both mobile devices and pc's
	var pos_x = (e.originalEvent.touches ? e.originalEvent.touches[0].pageX : e.pageX) - parent_offset.left;
	var pos_y = (e.originalEvent.touches ? e.originalEvent.touches[0].pageY : e.pageY) - parent_offset.top;

	if(paint)
	{
		addPoint(pos_x, pos_y);
	}
}

function endTraject(e)
{
	paint = false;
}

